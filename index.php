<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ozanan</title>

    <!-- Principal CSS do Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
        crossorigin="anonymous">


    <!-- Estilos customizados para esse template -->
    <link href="src/view/login.css" rel="stylesheet">
</head>

<body class="text-center">
    <form class="form-signin" method="POST" action="src/arquivos/Autentica.php">
        <img class="mb-4" src="src/img/logo_ozanan.png" alt="" width="50%" height="72">
        <h1 class="h3 mb-3 font-weight-normal">Faça login</h1>
        <?php
            if (isset($_GET['erro'])){
                echo '<div class="alert alert-danger" role="alert">
  '.$_GET['erro'].'
</div>';
            }
    
            if (isset($_GET['autentica'])){
                echo '<div class="alert alert-warning" role="alert">
  '.$_GET['autentica'].'
</div>';
            }
            ?>

        <label for="usuario" class="sr-only">Usuario</label>
        <input type="text" id="usuario" name="usuario" class="form-control" placeholder="Usuario" required autofocus>
        <label for="password" class="sr-only">Senha</label>
        <input type="password" id="password" name="senha" class="form-control" placeholder="Senha" required>
        <div class="checkbox mb-3">
        </div>
        <button class="btn btn-lg  btn-block" type="submit">Entrar</button>
        <p class="mt-5 mb-3 text-muted">&copy; Fapam - 2018</p>
    </form>
</body>

</html>