<?php

session_start();
if (!isset($_SESSION['usuario'])) {
// Usuário não logado! Redireciona para a página de login 
    header("Location: ../../index.php?autentica=Você precisa de altenticação");
    exit;
}

if (isset($_GET["fechar"]) && isset($_SESSION['usuario'])) {
    session_destroy();
    header("Location: ../../index.php");
    exit;
}
?> 