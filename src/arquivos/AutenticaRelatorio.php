<?php
session_start();
date_default_timezone_set("America/Sao_Paulo");
require_once '../model/UsuarioDao.php';
require_once '../config/Database.php';
$usuario = $_SESSION['usuario'];

$db = new Database();
$dao = $db->getConection();

$query = "INSERT INTO sessoes(login,data) VALUES(?,?)";
if ($stmt = $dao->prepare($query)) {
    $stmt->bind_param('ss', $usuario, date('Y-m-d H:i:s'));
    if ($stmt->execute()) {
        header("Location: http://localhost/relatorios/index.php?login=$usuario");
    } else {
        "Falha ao gravar sessão no banco de dados";
    }
}