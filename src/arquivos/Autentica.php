<?php
session_start();
require_once '../model/UsuarioDao.php';
require_once '../config/Database.php';
$usuario = $_POST['usuario'];
$senha = $_POST['senha'];
$db = new Database();
$dao = new UsuarioDao($db);
$usuario_logado = $dao->buscar($usuario, $senha);
if ($usuario_logado != false) {
     $_SESSION['usuario'] = $usuario_logado->getUsuario();
     $_SESSION['id_usuario'] = $usuario_logado->getId();
    header('Location: ../view/Painel.php');
} else {
    header("Location: ../../index.php?erro=Usuário ou senhas incorretos!");
    echo "erro";
}