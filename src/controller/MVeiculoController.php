<?php
require_once ('../model/Carro.php');
require_once ('../model/CarroDAO.php');
require_once ('../config/Database.php');
require_once ('../model/Pessoas.php');
require_once ('../model/PessoasDAO.php');
require_once "../model/MVeiculoDao.php";

new MVeiculoController($_GET['action']);

class MVeiculoController {
    
    public function  __construct($action){
        
        switch ($action) {
            case "adiciona":
                $this->insere();
                break;
            case "atualiza":
                $this->atualiza();
                break;
            case "remove":
                $this->remove();
                break;
            case "lista":
                $this->lista();
                break;
            case "remove":
                $this->remove();
                break;
            case "edita":
                $this->edita();
                break;
            default:
                echo "nada a fazer";
                break;
        }
        
    }
    
    public function remove(){
        $idcarro = $_POST['postcarro'];
        $db = new Database();
        $dao = new CarroDAO($db);
        if($dao->remove($idcarro))
        {
            echo true;
        }
    }
    
    public function atualiza(){

        $veiculo = new MVeiculo();
        $veiculo->setId($_POST['id']);
        $veiculo->setKmEntrda($_POST['kmEntrada']);
        $veiculo->setDentrada($_POST['data']);
        $db = new Database();
        $dao = new MVeiculoDao($db);
        if($dao->altera($veiculo))
        {
            echo "<script>alert('Fechado');window.location.href='MVeiculoController.php?action=lista';</script>";
        }
    }
    
    public function edita(){
        $idcarro = $_GET['idcarro'];
        $db = new Database();
        $dao = new CarroDAO($db);
        $carro = $dao->buscar($idcarro);
        include '../view/EditaCarro.php';
    }

    public function lista(){
        $db = new Database();
        $dao = new MVeiculoDao($db);
        $mVeiculo = $dao->buscar();
        include '../view/movimenta.php';

    }
    
    public function insere() {
        $carro = new Carro();
        $pessoa = new Pessoas();
        $pessoa->setId($_POST['idFun']);
        $carro->setId($_POST['idCar']);
        $motivo = $_POST['motivo'];
        $obs = $_POST['obs'];
        $data = $_POST['data'];
        $hora = $_POST['hora'];
        $kmSaida = $_POST['kmSaida'];
        $db = new Database();
        $dao = new MVeiculoDao($db);
        if($dao->add($pessoa,$carro,$kmSaida,$hora,$motivo,$data,$obs))
        {
        echo "<script>alert('Saida Realizada');window.location.href='MVeiculoController.php?action=lista';</script>";
        }else{
   echo "<script>alert('Algo errado aconteceu, desculpe');window.location.href='MVeiculoController.php?action=lista';</script>";
        }
    }

}