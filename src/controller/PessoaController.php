<?php
require_once ('../model/Email.php');
require_once ('../model/EmailDAO.php');
require_once ('../model/Endereco.php');
require_once ('../model/EnderecoDAO.php');
require_once ('../model/Telefone.php');
require_once ('../model/TelefoneDAO.php');
require_once ('../model/Email.php');
require_once ('../model/EmailDAO.php');
require_once ('../config/Database.php');
require_once ('../model/Pessoas.php');
require_once ('../model/PessoasDAO.php');

new PessoaController($_GET['action']);

class PessoaController{
     public function  __construct($action){
        
        switch ($action) {
            case "adiciona":
                $this->insere();
                break;
            case "atualiza":
                $this->atualiza();
                break;
            case "remove":
                $this->remove();
                break;
            case "lista":
                $this->lista();
                break;
            case "remove":
                $this->remove();
                break;
            case "edita":
                $this->edita();
                break;
			case "filtrar":
                $this->filtrar();
                break;
            default:
                echo "nada a fazer";
                break;
        }
        
    }
    
	public function filtrar(){
		$db = new Database();
        $dao = new PessoasDAO($db);
		$campo = $_POST['campo'];
		$filtroBusca = $_POST['busca'];
		if(empty($filtroBusca)){
			$pessoas = $dao->buscarTodos();
		} else {
			switch($campo){
				case "Nome":
					$pessoas = $dao->buscarPorNome($filtroBusca);
					break;
				case "Telefone":
					$pessoas = $dao->buscarPorTelefone($filtroBusca);
					break;
				case "Email":
					$pessoas = $dao->buscarPorEmail($filtroBusca);
					break;
			}
		}
		include '../view/ListaPessoas.php';
	}
	
    public function remove(){
		$idpessoa = $_POST['postpessoa'];
        $db = new Database();
        $dao = new PessoasDAO($db);
        $comunicaEndereco = new EnderecoDAO($db);
        $comunicaTelefone = new TelefoneDAO($db);
        $comunicaEmail = new EmailDAO($db);
		//REMOVENDO
		$comunicaEmail->remove($idpessoa);
        $comunicaEndereco->remove($idpessoa);
        $comunicaTelefone->remove($idpessoa);
        $dao->remove($idpessoa);
		
		
		$pessoas = $dao->buscarTodos();
        include '../view/ListaPessoas.php';
    }
    
    public function lista(){
        $db = new Database();
        $dao = new PessoasDAO($db);
        $pessoas = $dao->buscarTodos();
        include '../view/ListaPessoas.php';
    }
    
    public function edita(){
        $idpessoa = $_GET['idpessoa'];
        $db = new Database();
        $dao = new PessoasDAO($db);
        $pessoa = $dao->busca($idpessoa);
        include '../view/EditaPessoa.php';
    }
    
	public function atualiza(){
		$db = new Database();
        $pessoa = new Pessoas();
		
		$comunicaEndereco = new EnderecoDAO($db);
        $comunicaTelefone = new TelefoneDAO($db);
        $comunicaEmail = new EmailDAO($db);
		
		//pessoa
        $pessoa->setCpfcnpj($_POST['cpf']);
        $pessoa->setDatanasc($_POST['data']);
        $pessoa->setNome($_POST['nome']);
        $pessoa->setSexo($_POST['sexo']);
        $pessoa->setObs($_POST['obs']);
        $pessoa->setId($_POST['idPessoa']);
		
		$idCadastrado = $_POST['idPessoa'];
		
        $dao = new PessoasDAO($db);
        $dao->altera($pessoa);
		
		//telefone
		$maxTel = sizeof($_POST['tel']);
		for($i = 0; $i < $maxTel;$i++){
			$telefone = new Telefone();
			$telefone->setTelefone($_POST['tel'][$i]);
			$telefone->setId($_POST['idTel'][$i]);
			
			if($_POST['idTel'][$i] > 0){
				$comunicaTelefone->altera($telefone);
			} else {
				$comunicaTelefone->add($telefone, $idCadastrado);
			}
		}
                        
        //email
		$maxEmail = sizeof($_POST['email']);
		for($i = 0; $i < $maxEmail;$i++){
			$email = new Email();
			$email->setId($_POST['idEmail'][$i]);
			$email->setEmail($_POST['email'][$i]);
			
			if($_POST['idEmail'][$i] > 0){
				$comunicaEmail->altera($email);
			} else {
				$comunicaEmail->add($email, $idCadastrado);
			}
		}
		
		$max = sizeof($_POST['rua']);
		for($i = 0; $i < $max;$i++)
		{
			$registro = new Endereco();
			$registro->setId($_POST['idEndereco'][$i]);
			$registro->setEndereco($_POST['rua'][$i]);
			$registro->setNumero($_POST['numero'][$i]);
			$registro->setBairro($_POST['bairro'][$i]);
			$registro->setCidade($_POST['cidade'][$i]);
			$registro->setCep($_POST['cep'][$i]);
			$registro->setEstado($_POST['estado'][$i]);
				
			if($_POST['idEndereco'][$i] > 0){
				$comunicaEndereco->altera($registro);
			} else {
				$comunicaEndereco->add($registro, $idCadastrado);
			}	
		}
		
		echo "<script>alert('Atualização realizada');window.location.href='PessoaController.php?action=lista';</script>";
	}
	
    public function insere() {
            $db = new Database();
            $pessoa = new Pessoas();
            $telefone = new Telefone();
            $email = new Email();
            
            $comunicaEndereco = new EnderecoDAO($db);
            $comunicaTelefone = new TelefoneDAO($db);
            $comunicaEmail = new EmailDAO($db);
            
            //pessoa
            $pessoa->setCpfcnpj($_POST['cpf']);
            $pessoa->setDatanasc($_POST['data']);
            $pessoa->setNome($_POST['nome']);
            $pessoa->setSexo($_POST['sexo']);
            $pessoa->setObs($_POST['obs']);
            
            $dao = new PessoasDAO($db);
            $dao->add($pessoa);
            
            $idCadastrado = $dao->ultimaPessoa();
            
            //telefone
			foreach($_POST['tel'] as $tel){
				$telefone->setTelefone($tel);
				$comunicaTelefone->add($telefone, $idCadastrado);
			}
                        
            //email
			foreach($_POST['email'] as $RegistroEmail){
				$email->setEmail($RegistroEmail);
				$comunicaEmail->add($email, $idCadastrado);
			}
            
			
			$max = sizeof($_POST['rua']);
			for($i = 0; $i < $max;$i++)
			{
				$registro = new Endereco();
				$registro->setEndereco($_POST['rua'][$i]);
				$registro->setNumero($_POST['numero'][$i]);
				$registro->setBairro($_POST['bairro'][$i]);
				$registro->setCidade($_POST['cidade'][$i]);
				$registro->setCep($_POST['cep'][$i]);
				$registro->setEstado($_POST['estado'][$i]);
				$comunicaEndereco->add($registro, $idCadastrado);
			}
			
            
            echo "<script>alert('Cadastro realizado');window.location.href='PessoaController.php?action=lista';</script>";
     }
    
    
}