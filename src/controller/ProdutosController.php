<?php

require_once ('../model/Produtos.php');
require_once ('../model/ProdutosDAO.php');
require_once ('../config/Database.php');
$produtosController = new ProdutosController($_GET['action']);  
class ProdutosController {
    
    public function  __construct($action){
        
        switch ($action) {
            case "adiciona":
                $this->insere();
                break;
            case "lista":
                $this->lista();
                break;
            case "remove":
                $this->remove();
                break;
            case "atualiza":
                $this->atualiza();
                break;
            case "edita":
                $this->edita();
                break;
            case "filtrar":
                $this->filtrar();
                break;
            default:
                echo "nada a fazer";
                break;
        }
        
    }
    
    public function insere() {
        
        $produto = new Produtos();
        $produto->setNome($_POST['nome']);
        $produto->setClassificacao($_POST['classificacao']);
        $produto->setQuantidade_minima($_POST['quantidade_minima']);
        
        $db = new Database();
        $dao = new ProdutosDAO($db);
        $dao->add($produto);
        echo "<script>alert('Cadastrado com sucesso');window.location.href='ProdutosController.php?action=lista';</script>";
    }
    
    public function remove(){
        $idproduto = $_POST['postproduto'];
        $db = new Database();
        $dao = new ProdutosDAO($db);
        if($dao->remove($idproduto))
        {
            echo true;
        }
    }
    
    public function atualiza(){
        $produto = new Produtos();
        $produto->setId($_POST['id']);
        $produto->setNome($_POST['nome']);
        $produto->setClassificacao($_POST['classificacao']);
        $produto->setQuantidade_minima($_POST['quantidade_minima']);
        $db = new Database();
        $dao = new ProdutosDAO($db);
        if($dao->atualiza($produto))
        {
            echo "<script>alert('Atualização realizada');window.location.href='ProdutosController.php?action=lista';</script>";
        }else{
            "<script>alert('Cancelado')window.location.href='ProdutosController.php?action=lista';</script>";
        }
    }
    
    public function edita(){
        $idproduto = $_GET['idproduto'];
        $db = new Database();
        $dao = new ProdutosDAO($db);
        $produto = $dao->buscar($idproduto);
        include '../view/EditaProduto.php';
    }
    
    public function lista(){
        $db = new Database();
        $dao = new ProdutosDAO($db);
        $produtos = $dao->buscarTodos();
        include '../view/ListaProdutos.php';
        
    }
    public function filtrar(){
        $db = new Database();
        $dao = new ProdutosDAO($db);
        $produtos = $dao->filtrar($_POST['busca']);
        include '../view/ListaProdutos.php';
        
    }
}