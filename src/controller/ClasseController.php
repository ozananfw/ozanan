<?php
require_once ('../model/Classe.php');
require_once ('../model/ClasseDAO.php');
require_once ('../config/Database.php');

new ClasseController($_GET['action']);

class ClasseController {
    
    public function  __construct($action){
        
        switch ($action) {
            case "adiciona":
                $this->insere();
                break;
            case "lista":
                $this->lista();
            default:
                echo "nada a fazer";
                break;
        }
        
    }
    
    public function insere() {
        $classe = new Classe();
        $classe->setDescricao($_POST['descricao']);
        $db = new Database();
        $dao = new ClasseDAO($db);
        if($dao->add($classe)) {
            echo true;
            echo "window.location='../view/form_medicamentos.php'";
        } else {
            echo false;
        }
    }
    
    public function lista(){
        $db = new Database();
        $dao = new ClasseDAO($db);
        $classes = $dao->buscarTodos();
        $ArrayClasses = "";
        foreach($classes as $class) {
            $ArrayClasses .= "<option value='{$class->getId()}'>{$class->getDescricao()}</option>";
        }
        echo $ArrayClasses;
    }
   
    
}