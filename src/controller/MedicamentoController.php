<?php

require_once ('../model/Medicamento.php');
require_once ('../model/MedicamentoDAO.php');
require_once ('../config/Database.php');
$medicamentoController = new MedicamentoController($_GET['action']);
class MedicamentoController {
    
    public function  __construct($action){
        
        switch ($action) {
            case "adiciona":
                $this->insere();
                break;
            case "lista":
                $this->lista();
                break;
            case "remove":
                $this->remove();
                break;
            case "atualiza":
                $this->atualiza();
                break;
            case "edita":
                $this->edita();
                break;
            case "filtrar":
                $this->filtrar();
                break;
            default:
                echo "nada a fazer";
                break;
        }
        
    }
    
    public function insere() {
        $prod = new Produtos();
        $prod->setNome($_POST['nome']);
        $prod->setClassificacao(4);
        $prod->setQuantidade_minima($_POST['quantidade_minima']);
        $db = new Database();
        $produtoDAO = new ProdutosDAO($db);
         if($produtoDAO->add($prod)){
//             echo "<script>alert('entrou');</script>";
             $id = mysqli_insert_id($db->getConection());
             $med = new Medicamento();
             $med->setClasse($_POST['classe']);
             $med->setDosagem($_POST["dosagem"]);
             $dao = new MedicamentoDAO($db);
             if($dao->add($med,$id)){
                 echo "<script>alert('Cadastrado com sucesso');window.location.href='MedicamentoController.php?action=lista';</script>";
             }
         }
    }
    
    public function remove(){
        $idmed = $_POST['postmed'];
        $db = new Database();
        $dao = new MedicamentoDAO($db);
        if($dao->remove($idmed)) {
            echo true;
            echo "window.location='../view/ListaMedicamentos.php'";
        }
    }
    
    public function atualiza(){
        $medicamento = new Medicamento();
        $medicamento->setId($_POST['id']);
        $medicamento->setNome($_POST['nome']);
        $medicamento->setQuantidade_minima($_POST['quantidade_minima']);
        $medicamento->setClasse($_POST["classe"]);
        $medicamento->setDosagem($_POST["dosagem"]);
        $medicamento->setClassificacao(4);
        $db = new Database();
        $dao = new MedicamentoDAO($db);
        if($dao->atualiza($medicamento))
        {
            echo "<script>alert('Atualização realizada');window.location.href='MedicamentoController.php?action=lista';</script>";
        } else{
            "<script>alert('Cancelado')window.location.href='MedicamentoController.php?action=lista';</script>";
        }
    }
    
    public function edita(){
        $idMedicamento = $_GET['idmed'];
        $db = new Database();
        $dao = new MedicamentoDAO($db);
        $medicamento = $dao->buscar($idMedicamento);
        include '../view/EditaMedicamento.php';
    }
    
    public function lista(){
        $db = new Database();
        $dao = new MedicamentoDAO($db);
        $medicamentos = $dao->buscarTodos();
        include '../view/ListaMedicamentos.php';
        
    }
    public function filtrar(){
        $db = new Database();
        $dao = new MedicamentoDAO($db);
        $medicamentos = $dao->filtrar($_POST['busca'],$_POST["campo"]);
        include '../view/ListaMedicamentos.php';
        
    }
}