<?php

require_once ('../config/Database.php');
require_once ('../model/Usuario.php');
require_once ('../model/UsuarioDAO.php');


new UsuarioController($_GET['action']);

class UsuarioController {

    public function __construct($action) {

        switch ($action) {
            case "adiciona":
                $this->insere();
                break;
            case "lista":
                $this->lista();
            case "remove":
                $this->remove();
        }
    }

    public function insere() {


        $usuario = new Usuario();
        $usuario->setUsuario($_POST['usuario']);
        $usuario->setSenha($_POST['senha']);
        $usuario->setId_pessoa($_POST['idpessoa']);
        $db = new Database();
        $dao = new UsuarioDAO($db);

        if($dao->verifica($_POST['usuario'])){
            echo "<script>alert('teste1');</script>";
            echo "<script>alert('Usuario já existe!');window.location.href='UsuarioController.php?action=lista';</script>";
        }else{
            if($dao->add($usuario)){
                echo "<script>alert('Usuario cadastrado');window.location.href='UsuarioController.php?action=lista';</script>";
            }
        }

    }

    public function lista() {
        $db = new Database();
        $dao = new UsuarioDao($db);
        $usuarios = $dao->buscarTodos();
        include '../view/Usuario_view.php';
    }

    public function remove() {
        if(isset($_POST["postusuario"])) {
            $idusuario = $_POST['postusuario'];
            $db = new Database();
            $dao = new UsuarioDao($db);
            if ($dao->remove($idusuario)) {
                echo true;
            }
        }
    }
    
    public function atualiza(){
        $usuario = new Usuario();
        $usuario->setId($_POST['id']);
        $usuario->setUsuario($_POST['usuario']);
        $usuario->setSenha($_POST['senha']);
        $db = new Database();
        $dao = new CarroDAO($db);
        if($dao->atualiza($carro))
        {
            echo "<script>alert('Atualiza??o realizada');window.location.href='CarroController.php?action=lista';</script>";
        }
    }


}
