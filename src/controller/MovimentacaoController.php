<?php
require_once ('../model/Movimentacao.php');
require_once ('../model/MovimentacaoDAO.php');
require_once ('../model/MovimentacaoHasProdutoDAO.php');
//require_once ('../model/Produto_movimentacao.php');
require_once ('../config/Database.php');
$movimentacaoController = new MovimentacaoController($_GET['action']);
class MovimentacaoController {
    
    public function  __construct($action){
        
        switch ($action) {
            case "adiciona":
                $this->insere();
                break;
            case "lista":
                $this->lista();
                break;
            case "remove":
                $this->remove();
                break;
            case "atualiza":
                $this->atualiza();
                break;
            case "edita":
                $this->edita();
                break;
            case "filtrar":
                $this->filtrar();
                break;
            default:
                echo "nada a fazer";
                break;
        }
        
    }
    
    public function insere() {
        $movimentacao = new Movimentacao();
        $movimentacao->setData($_POST['data']);
        $movimentacao->setOrigem($_POST['origem']);
        $movimentacao->setDoador_id($_POST['doadores']);
        $movimentacao->setFuncionario_id($_POST['id_usuario']);
        $movimentacao->setSolicitante_id($_POST["solicitante"]);
        $movimentacao->setTipo($_POST['tipo']);
        $db = new Database();
        $dao = new MovimentacaoDAO($db);
        $dao->add($movimentacao);
        $id = mysqli_insert_id($db->getConection());
//        echo "<script>alert($id);</script>";
            foreach ($_POST["produto"] as $key => $prod) {
                $produto = new Produtos();
                $produto->setId($prod);
                $produto->setQuantidade_movimentacao($_POST["quantidade"][$key]);
                $mov_produto = new MovimentacaoHasProdutoDAO($db);
                $mov_produto->add($produto, $id);
            }
        echo "<script>alert('Cadastrado com sucesso');window.location.href='MovimentacaoController.php?action=lista';</script>";
        
    }
    
    
    public function remove(){
        $idmovimentacao = $_POST['postmovimentacao'];
        $db = new Database();
        $dao = new MovimentacaoDAO($db);
        if($dao->remove($idmovimentacao)){
            echo true;
        }
    }
    
    public function atualiza(){
        $movimentacao = new Movimentacao();
        $movimentacao->setData($_POST['data']);
        $movimentacao->setOrigem($_POST['origem']);
        $movimentacao->setDoador_id($_POST['doadores']);
        $movimentacao->setFuncionario_id($_POST['id_usuario']);
        $movimentacao->setSolicitante_id($_POST["solicitante"]);
        $movimentacao->setTipo($_POST['tipo']);
        $movimentacao->setId($_POST['id']);
        $db = new Database();
        $dao = new MovimentacaoDAO($db);
        if($dao->atualiza($movimentacao)) {
            echo "<script>alert('Atualização realizada');window.location.href='MovimentacaoController.php?action=lista';</script>";
        } else{
            "<script>alert('Cancelado')window.location.href='MovimentacaoController.php?action=lista';</script>";
        }
    }
    
    public function edita(){
        $idMovimentacao = $_GET['idmovimentacao'];
        $db = new Database();
        $dao = new MovimentacaoDAO($db);
        $movimentacao = $dao->buscar($idMovimentacao);
        include '../view/EditaMovimentacao.php';
    }
    
    public function lista(){
        $db = new Database();
        $dao = new MovimentacaoDAO($db);
        $movimentacoes = $dao->buscarTodos();
        include '../view/ListaMovimentacao.php';
        
    }
    public function filtrar(){
        $db = new Database();
        $dao = new MovimentacaoDAO($db);
        if($_POST['busca'] =="") {
            $movimentacoes = $dao->buscarTodos();
        } else{
            $movimentacoes = $dao->filtrar($_POST['busca'], $_POST["campo"]);
        }
        include '../view/ListaMovimentacao.php';
        
    }
}