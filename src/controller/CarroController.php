<?php
require_once ('../model/Carro.php');
require_once ('../model/CarroDAO.php');
require_once ('../config/Database.php');

new CarroController($_GET['action']);

class CarroController {
    
    public function  __construct($action){
        
        switch ($action) {
            case "adiciona":
                $this->insere();
                break;
            case "atualiza":
                $this->atualiza();
                break;
            case "remove":
                $this->remove();
                break;
            case "lista":
                $this->lista();
                break;
            case "remove":
                $this->remove();
                break;
            case "edita":
                $this->edita();
                break;
            default:
                echo "nada a fazer";
                break;
        }
        
    }
    
    public function remove(){
        $idcarro = $_POST['postcarro'];
        $db = new Database();
        $dao = new CarroDAO($db);
        if($dao->remove($idcarro))
        {
            echo true;
        }
    }
    
//    public function atualiza(){
//        $carro = new Carro();
//        $carro->setId($_POST['id']);
//        $carro->setNome($_POST['nome']);
//        $carro->setPlaca($_POST['placa']);
//        $db = new Database();
//        $dao = new CarroDAO($db);
//        if($dao->atualiza($carro))
//        {
//            echo "<script>alert('Atualiza??o realizada');window.location.href='CarroController.php?action=lista';</script>";
//        }
//    }
    
    public function edita(){
        $idcarro = $_GET['idcarro'];
        $db = new Database();
        $dao = new CarroDAO($db);
        $carro = $dao->buscar($idcarro);
        include '../view/EditaCarro.php';
    }
    
    public function lista(){
        $db = new Database();
        $dao = new CarroDAO($db);
        $carros = $dao->buscarTodos();
        include '../view/ListaCarros.php';
    }
    
    public function insere() {
        $carro = new Carro();
        $carro->setNome($_POST['nome']);
        $carro->setPlaca($_POST['placa']);
        $carro->setStatus('P');
        $db = new Database();
        $dao = new CarroDAO($db);
        if($dao->add($carro))
        {
        echo "<script>alert('Cadastro realizado');window.location.href='CarroController.php?action=lista';</script>";
        }else{
            echo "<script>alert('Algo errado aconteceu, desculpe');window.location.href='CarroController.php?action=lista';</script>";
        }
    }
    
   
    
}