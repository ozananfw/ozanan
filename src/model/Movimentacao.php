<?php
class Movimentacao {
    
    private $id;
    private $produtos = array();
    private $data;
    private $origem;
    private $doador_id;
    private $funcionario_id;
    private $solicitante_id;
    private $tipo;
    private $quantidade;

    function getSolicitante_id() {
        return $this->solicitante_id;
    }

    function setSolicitante_id($solicitante_id) {
        $this->solicitante_id = $solicitante_id;
    }

    public function getTipo() {
        return $this->tipo;
    }
    function getProdutos() {
        return $this->produtos;
    }
    function getQuantidade() {
        return $this->quantidade;
    }

    function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;
    }

        function setProdutos($produtos) {
        $this->produtos = $produtos;
    }

    public function setTipo($tipo){
        $this->tipo = $tipo;
    }

    function getId() {
        return $this->id;
    }

    function getData() {
        return $this->data;
    }

    function getOrigem() {
        return $this->origem;
    }

    function getDoador_id() {
        return $this->doador_id;
    }

    function getFuncionario_id() {
        return $this->funcionario_id;
    }

    function setId($id) {
        $this->id = $id;
    }


    function setData($data) {
        $this->data = $data;
    }

    function setOrigem($origem) {
        $this->origem = $origem;
    }

    function setDoador_id($doador_id) {
        $this->doador_id = $doador_id;
    }

    function setFuncionario_id($funcionario_id) {
        $this->funcionario_id = $funcionario_id;
    }

}

