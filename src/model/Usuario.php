<?php

require_once '../model/Pessoas.php';

class Usuario extends Pessoas {

    private $id;
    private $id_pessoa;
    private $usuario;
    private $senha;
    
    function __construct() {
    
        parent::__construct();
        
    }
    
    function getId() {
        return $this->id;
        
    }

    function getId_pessoa() {
        return $this->id_pessoa;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getSenha() {
        return $this->senha;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setId_pessoa($id_pessoa) {
        $this->id_pessoa = $id_pessoa;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

}

?>
