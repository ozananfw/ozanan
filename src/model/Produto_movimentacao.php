<?php
class Produto_movimentacao
{
    private $produto;
    private $quantidade;
    private $movimentacap_id;
    
    /**
     * @return mixed
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @return mixed
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @return mixed
     */
    public function getMovimentacap_id()
    {
        return $this->movimentacap_id;
    }

    /**
     * @param mixed $produto
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
    }

    /**
     * @param mixed $quantidade
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
    }

    /**
     * @param mixed $movimentacap_id
     */
    public function setMovimentacap_id($movimentacap_id)
    {
        $this->movimentacap_id = $movimentacap_id;
    }

    
    
}