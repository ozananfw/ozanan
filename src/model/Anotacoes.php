<?php
class Anotacoes {  
    private $id;  
    private $mensagem;  
    private $titulo;
    
    
    public function getId()
    {
        return $this->id;
    }

    public function getMensagem()
    {
        return $this->mensagem;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setMensagem($mensagem)
    {
        $this->mensagem = $mensagem;
    }

    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }
}  
?>