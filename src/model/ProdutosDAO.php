<?php
//use mvc\Produtos;

require_once ('../model/Produtos.php');
class ProdutosDAO
{
    private $mysqli;
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
    }
    
    public function buscar($idproduto){
        
        $query = "select id, nome, classificacao_id, quantidade_minima from produto where id=?";
        
        if ($stmt = $this->mysqli->prepare($query)) {
            
            $stmt->bind_param('i', $idproduto);
            
            $stmt->execute();
            
            $stmt->bind_result($id, $nome, $classificacao, $quantidade_minima);
            
            while ($stmt->fetch()) {
                
                $produto = new Produtos();
                
                $produto->setId($id);
                
                $produto->setNome($nome);
                
                $produto->setClassificacao($classificacao);
                
                $produto->setQuantidade_minima($quantidade_minima);
                
            }
            
            return $produto;
            
            $stmt->close();
            
        }
        
    }
    

    public function filtrar($nome){
        $query = "select id, nome, classificacao_id, quantidade_minima from produto where nome like '%$nome%'";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $nome, $classificacao, $quantidade_minima);
            while ($stmt->fetch()) {
                $produto = new Produtos();
                $produto->setId($id);
                $produto->setNome($nome);
                $produto->setClassificacao($classificacao);
                $produto->setQuantidade_minima($quantidade_minima);
                $produtos[] = $produto;    
            }
            return $produtos;
            $stmt->close();
        }
    }
    
    
    public function buscarTodos(){
        $produtos = array();
        $query = "select id, nome, quantidade_minima, classificacao_id from produto";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $nome, $quantidade_minima, $classificacao_id);
            while ($stmt->fetch()) {
                $produto = new Produtos();
                $produto->setId($id);
                $produto->setNome($nome);
                $produto->setClassificacao($classificacao_id);
                $produto->setQuantidade_minima($quantidade_minima);
                array_push($produtos, $produto);
            }
            return $produtos;
            $stmt->close();
        }
    }
    
    
    public function add(Produtos $produto)
    {
        mysqli_report(MYSQLI_REPORT_ALL);
        $nome = $produto->getNome();
        $classificacao = $produto->getClassificacao();
        $quantidade_minima = $produto->getQuantidade_minima();
//        echo "<script>alert('$nome');</script>";
//        echo "<script>alert('$classificacao');</script>";
//        echo "<script>alert('$quantidade_minima');</script>";
        $query = "INSERT INTO produto (nome, quantidade_minima, classificacao_id) VALUES(?,?,?)";
       if($stmt = $this->mysqli->prepare($query)){
            $stmt->bind_param('sii', $nome,$quantidade_minima,$classificacao);
        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
       }else{
           return false;
              
       }
        
       
        $stmt->close();
    }
   
    public function remove($idproduto){
        $query = "delete from produto where id =?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('i', $idproduto);
        if($stmt->execute())
        {
            return true;
        }else{
            return false;
        }
        $stmt->close();
        
    }
    public function atualiza($produto){
        $id = $produto->getId();
        $nome = $produto->getNome();
        $classificacao = $produto->getClassificacao();
        $quantidade_minima = $produto->getQuantidade_minima();
        $query = "update produto set nome='$nome', classificacao_id=$classificacao, quantidade_minima=$quantidade_minima where id =$id";
        if($stmt = $this->mysqli->prepare($query)){
            if($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
        $stmt->close();
        
    }
}