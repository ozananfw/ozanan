<?php
require_once '../model/Produtos.php';

class Medicamento extends Produtos{
    
    function __construct() {
    
        parent::__construct();
        
    }

    
    private $classe;
    private $dosagem;
	private $medicamento_id;
	
	function getMedicamento_id() {
        return $this->medicamento_id;
    }
	
	function setMedicamento_id($medicamento_id) {
        $this->medicamento_id = $medicamento_id;
    }
	
    function getClasse() {
        return $this->classe;
    }

    function getDosagem() {
        return $this->dosagem;
    }

    function setClasse($classe) {
        $this->classe = $classe;
    }

    function setDosagem($dosagem) {
        $this->dosagem = $dosagem;
    }

}

