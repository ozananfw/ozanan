<?php
require_once ('../model/Produtos.php');
require_once ('../model/ProdutosDAO.php');
class MovimentacaoHasProdutoDAO
{
    private $mysqli;
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
    }
    
    public function buscar($idMovimentacao){
        $db = new Database();
        $query = "select produto_id, quantidade from movimentacao_has_produto where movimentacao_id =?";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param('i', $idMovimentacao);
            $stmt->execute();
            $stmt->bind_result($produto_id, $quantidade);
            $array_produtos = array();
            while ($stmt->fetch()) {
                $prodDAO = new ProdutosDAO($db);
                $produto = $prodDAO->buscar($produto_id);
                $produto->setQuantidade_movimentacao($quantidade);
                array_push($array_produtos, $produto);
            }
            return $array_produtos;
            $stmt->close();
        }
    }
    
    public function add($produto, $id_movimentacao) {
        $produto_id = $produto->getId();
        $quantidade = $produto->getQuantidade_movimentacao();
        $query = "INSERT INTO movimentacao_has_produto (movimentacao_id, produto_id, quantidade) VALUES(?,?,?)";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('iii', $id_movimentacao, $produto_id, $quantidade);
        $stmt->execute();
        $stmt->close();
    }
    
    function remove($id_movimentacao) {
        $query = "delete from movimentacao_has_produto where movimentacao_id = ?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('i', $id_movimentacao);
        if($stmt->execute()){  
            return true;
        } else {
            return false;
        }
        $stmt->close();
    }

}