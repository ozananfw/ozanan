<?php
class Pessoas{
    private $id;
    private $nome;
    private $cpfcnpj;
    private $datanasc;
    private $sexo;
    private $obs;
    private $enderecos;
    private $telefones;
    private $email = array();
    
    function __construct() {
        
    }

    public function getTelefones()
    {
        return $this->telefones;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setTelefones($telefones)
    {
        $this->telefones = $telefones;
    }

    public function setEmail($email)
    {
        //array_push($this->email, $email);
		$this->email = $email;
    }

    public function getEnderecos()
    {
        return $this->enderecos;
    }

    public function setEnderecos($enderecos)
    {
        $this->enderecos = $enderecos;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getCpfcnpj()
    {
        return $this->cpfcnpj;
    }

    public function getDatanasc()
    {
        return $this->datanasc;
    }

    public function getSexo()
    {
        return $this->sexo;
    }

    public function getObs()
    {
        return $this->obs;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    public function setCpfcnpj($cpfcnpj)
    {
        $this->cpfcnpj = $cpfcnpj;
    }

    public function setDatanasc($datanasc)
    {
        $this->datanasc = $datanasc;
    }

    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    public function setObs($obs)
    {
        $this->obs = $obs;
    }

    
}