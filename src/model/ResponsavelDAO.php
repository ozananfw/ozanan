<?php
require_once ('../model/Pessoas.php');
require_once ('../model/Idoso.php');
class ResponsavelDAO
{
    private $mysqli;
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
    }
    
    public function add(Pessoas $pessoa, $id_idoso)
    {
		$responsavel = $pessoa->getId();
		
        $query = "INSERT INTO responsavel (idoso_id, pessoa_id) VALUES(?,?)";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('ii', $id_idoso, $responsavel);
        $stmt->execute();
        $stmt->close();
    }
	
    public function remove($id_pessoa, $id_idoso){
        $query = "DELETE FROM responsavel WHERE idoso_id = ? and pessoa_id = ?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('ii', $id_pessoa, $id_idoso);
        $stmt->execute();
        $stmt->close();
    }
	
	function exibirData($data){
		$rData = explode("-", $data);
		$rDataFormatada = $rData[2].'/'.$rData[1].'/'.$rData[0];
		return $rDataFormatada;
	}
    
    public function buscaPorIdoso($id_idoso){
        $responsaveis = array();
        $query = "select id, nome, data_nascimento, sexo, obs, cpf from pessoa inner join responsavel on responsavel.pessoa_id = pessoa.id where idoso_id = ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->bind_param('i', $id_idoso);
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $data_nascimento, $sexo, $obs, $cpf);
            /* fetch values */
            while ($stmt->fetch()) {
                $registro = new Pessoas();
                $registro->setId($id);
                $registro->setNome($nome);
                $dtFormatada = $this->exibirData($data_nascimento);
                $registro->setDatanasc($dtFormatada);
                $registro->setSexo($sexo);
                $registro->setObs($obs);
                $registro->setCpfcnpj($cpf);
                               
                array_push($responsaveis, $registro);
            }
            return $responsaveis;
            /* close statement */
            $stmt->close();
        }
    }
    
}