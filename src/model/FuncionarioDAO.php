<?php
require_once ('../model/Funcionario.php');

class FuncionarioDAO
{
    private $mysqli;
    public function __construct(Database $db){
        $this->mysqli = $db->getConection();
    }
    
	
    public function buscar($id_funcionario){
        $query = "SELECT f.id, p.nome FROM pessoa p INNER JOIN funcionario f ON f.pessoa_id = p.id WHERE pessoa_id = ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param('i', $id_funcionario);
            $stmt->execute();
            $stmt->bind_result($id, $nome);
            $funcionario = new Funcionario();
            while ($stmt->fetch()) {
                $funcionario->setId($id);
                $funcionario->setNome($nome);
            }
            return $funcionario;
            $stmt->close();
        }
    }



}