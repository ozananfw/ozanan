<?php
require_once ('../model/Telefone.php');
class TelefoneDAO
{
    private $mysqli;
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
    }
    
    public function add(Telefone $telefone, $id_pessoa)
    {
        $registro= $telefone->getTelefone();
        $pessoa_id = $id_pessoa;
        
        $query = "INSERT INTO telefone (telefone, pessoa_id) VALUES(?,?)";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('si', $registro, $pessoa_id);
        $stmt->execute();
        $stmt->close();
    }
    
	public function altera(Telefone $telefone)
    {
        $registro= $telefone->getTelefone();
        $id = $telefone->getId();
        
        $query = "UPDATE telefone SET telefone=? WHERE id=?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('si', $registro, $id);
        $stmt->execute();
        $stmt->close();
    }
	
    public function remove($pessoaID){
        $query = "DELETE FROM telefone WHERE pessoa_id = ?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('i', $pessoaID);
        $stmt->execute();
        $stmt->close();
    }
    
    public function buscaPorPessoa($idPessoa){
        $telefones = array();
        $query = "select id, telefone, pessoa_id from telefone where pessoa_id = ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->bind_param('i', $idPessoa);
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $telefone, $pessoaID);
            /* fetch values */
            while ($stmt->fetch()) {
                $registro = new Telefone();
                $registro->setId($id);
                $registro->setPessoaID($pessoaID);
                $registro->setTelefone($telefone);
                
                array_push($telefones, $registro);
            }
            return $telefones;
            /* close statement */
            $stmt->close();
        }
    }
    
}