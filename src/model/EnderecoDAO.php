<?php
require_once ('../model/Endereco.php');

class EnderecoDAO
{
    private $mysqli;
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
    }
    
    public function add(Endereco $endereco, $id_pessoa)
    {
        $logradouro= $endereco->getEndereco();
        $numero = $endereco->getNumero();
        $bairro = $endereco->getBairro();
        $cidade = $endereco->getCidade();
        $estado = $endereco->getEstado();
        $cep = $endereco->getCep();
        $pessoaID = $id_pessoa;
        
        $query = "INSERT INTO endereco (endereco, numero, bairro, cidade, estado, cep, pessoa_id) VALUES(?,?,?,?,?,?,?)";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('ssssssi', $logradouro, $numero, $bairro, $cidade, $estado, $cep, $pessoaID);
        $stmt->execute();
        $stmt->close();
    }
    
	public function altera(Endereco $endereco)
    {
        $logradouro= $endereco->getEndereco();
        $numero = $endereco->getNumero();
        $bairro = $endereco->getBairro();
        $cidade = $endereco->getCidade();
        $estado = $endereco->getEstado();
        $cep = $endereco->getCep();
		$id = $endereco->getId();
        
        $query = "UPDATE endereco SET endereco=?, numero=?, bairro=?, cidade=?, estado=?, cep= ? WHERE id = ?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('ssssssi', $logradouro, $numero, $bairro, $cidade, $estado, $cep, $id);
        $stmt->execute();
        $stmt->close();
    }
	
    public function remove($pessoaID){
        $query = "DELETE FROM endereco WHERE pessoa_id = ?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('i', $pessoaID);
        $stmt->execute();
        $stmt->close();
    }
    
    public function buscaPorIDPessoa($id_pessoa){
            $enderecos = array();
            $idRecebido = $id_pessoa;
            $query = "select id, endereco, numero, bairro, cidade, estado, cep, pessoa_id from endereco where pessoa_id = ?";
            if ($stmt = $this->mysqli->prepare($query)) {
                $stmt->bind_param('i', $idRecebido);
                $stmt->execute();
                /* bind result variables */
                $stmt->bind_result($id, $logradouro, $numero, $bairro, $cidade, $estado, $cep, $pessoaID);
                /* fetch values */
                while ($stmt->fetch()) {
                    $registro = new Endereco();
                    $registro->setId($id);
                    $registro->setEndereco($logradouro);
                    $registro->setNumero($numero);
                    $registro->setBairro($bairro);
                    $registro->setCidade($cidade);
                    $registro->setEstado($estado);
                    $registro->setCep($cep);
                    $registro->setPessoaID($pessoaID);
                    
                    array_push($enderecos, $registro);
                }
            }
            return $enderecos;
            /* close statement */
            $stmt->close();
        }
    }