<?php
class Telefone{
    private $id;
    private $telefone;
    private $pessoaID;
    
    
    public function getPessoaID()
    {
        return $this->pessoaID;
    }

    public function setPessoaID($pessoaID)
    {
        $this->pessoaID = $pessoaID;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTelefone()
    {
        return $this->telefone;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }
}