<?php
require_once ('../model/Email.php');
class EmailDAO
{
    private $mysqli;
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
    }
    
    public function add(Email $email, $id_pessoa)
    {
        $emails= $email->getEmail();
        $pessoa_id = $id_pessoa;
        
        $query = "INSERT INTO email (email, pessoa_id) VALUES(?,?)";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('si', $emails, $pessoa_id);
        $stmt->execute();
        $stmt->close();
    }
    
	public function altera(Email $email)
    {
        $emails= $email->getEmail();
        $id = $email->getId();
        
        $query = "UPDATE email SET email=? WHERE id=?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('si', $emails, $id);
        $stmt->execute();
        $stmt->close();
    }
	
    public function remove($pessoaID){
        $query = "DELETE FROM email WHERE pessoa_id = ?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('i', $pessoaID);
        $stmt->execute();
        $stmt->close();
    }
    
    public function buscaPorPessoa($id_pessoa){
        $emails = array();
        $query = "select id, email, pessoa_id from email where pessoa_id = ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->bind_param('i', $id_pessoa);
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $email, $pessoa_id);
            /* fetch values */
            while ($stmt->fetch()) {
                $registro = new Email();
                $registro->setId($id);
                $registro->setEmail($email);
                $registro->setIdpessoa($pessoa_id);
                
                array_push($emails, $registro);
            }
            return $emails;
            /* close statement */
            $stmt->close();
        }
    }
    
}