<?php
require_once ('../model/Pessoas.php');
require_once ('../model/EnderecoDAO.php');
require_once ('../model/EmailDAO.php');
require_once ('../model/TelefoneDAO.php');
require_once ('../model/Endereco.php');
require_once ('../model/Telefone.php');
require_once ('../model/Email.php');
require_once ('../model/Idoso.php');

class IdosoDAO{
    private $mysqli;
    private $ParametroDb;
    
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
        $this->ParametroDb = $db;
    }
    
    public function remove($pessoaID){
		$id = $pessoaID;
        $query = "DELETE FROM idoso WHERE pessoa_id = ?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->close();
    }
   
    public function add($id_pessoa)
    {
        $query = "INSERT INTO idoso (pessoa_id) VALUES(?)";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('i', $id_pessoa);
        $stmt->execute();
        $stmt->close();
    }
	
	function exibirData($data){
		$rData = explode("-", $data);
		$rDataFormatada = $rData[2].'/'.$rData[1].'/'.$rData[0];
		return $rDataFormatada;
	}
	
	public function addResponsavel($ididoso, $idresponsavel){
		$query = "INSERT INTO responsavel (idoso_id, pessoa_id) VALUES(?,?)";
		$stmt = $this->mysqli->prepare($query);
		$stmt->bind_param('ii', $ididoso, $idresponsavel);
		$stmt->execute();
        $stmt->close();
	}
	
	public function addMedicamento($ididoso, $idMedicamento){
		$query = "INSERT INTO idoso_has_medicamento (idoso_id, medicamento_id) VALUES(?,?)";
		$stmt = $this->mysqli->prepare($query);
		$stmt->bind_param('ii', $ididoso, $idMedicamento);
		$stmt->execute();
        $stmt->close();
	}
	
	public function removeMedicamento($id_pessoa, $idMedicamento){
        $query = "DELETE FROM idoso_has_medicamento WHERE idoso_id = ? and medicamento_id = ?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('ii', $id_pessoa, $idMedicamento);
        $stmt->execute();
        $stmt->close();
    }
	
	public function altera(Pessoas $pessoa)
    {        
        $nome= $pessoa->getNome();
        $cpfcnpj = $pessoa->getCpfcnpj();
        $datanasc = $pessoa->getDatanasc();
		$data = $this->formatarData($datanasc);
        $sexo = $pessoa->getSexo();
        $obs = $pessoa->getObs();
        $id = $pessoa->getId();
        
        $query = "UPDATE pessoa SET nome=?, data_nascimento=?, sexo=?, obs=?, cpf=? WHERE id=?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('sssssi', $nome, $datanasc, $sexo, $obs, $cpfcnpj, $id);
        $stmt->execute();
        $stmt->close();
        
    }
    public function buscaPorIdoso($idoso_id){
  
        
        $query = "select pessoa.id, nome, data_nascimento, sexo, obs, cpf from pessoa inner join idoso on idoso.pessoa_id = pessoa.id where idoso.id = ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param('i', $idoso_id);
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $data_nascimento, $sexo, $obs, $cpf);
            /* fetch values */
            while ($stmt->fetch()) {
				
                $registro = new Pessoas();
                $registro->setId($id);
                $registro->setNome($nome);
				$dtFormatada = $this->exibirData($data_nascimento);
                $registro->setDatanasc($dtFormatada);
                $registro->setSexo($sexo);
                $registro->setObs($obs);
                $registro->setCpfcnpj($cpf);
                
                return $registro;
            }
            /* close statement */
            $stmt->close();
        }
    }
        
    public function buscarTodos(){
        $idosos= array();
        
        $query = "select idoso.id, pessoa.nome, idoso.pessoa_id from pessoa inner join idoso on pessoa.id = idoso.pessoa_id";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $pessoa_id);
            /* fetch values */
            while ($stmt->fetch()) {
                
                $registro = new Idoso();
                $registro->setId($id);
                $registro->setNome($nome);
                $registro->setPessoa_id($pessoa_id);
				
                array_push($idosos, $registro);
            }
            return $idosos;
            /* close statement */
            $stmt->close();
        }
    }
	
	public function buscarPorNome($filtro){
        $pessoas= array();
        $comunicaEndereco = new EnderecoDAO($this->ParametroDb);
        $comunicaTelefone = new TelefoneDAO($this->ParametroDb);
        $comunicaEmail = new EmailDAO($this->ParametroDb);
        
        $query = "select id, nome, data_nascimento, sexo, obs, cpf from pessoa where nome like '%$filtro%'";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $data_nascimento, $sexo, $obs, $cpf);
            /* fetch values */
            while ($stmt->fetch()) {
                
                
                $emails = $comunicaEmail->buscaPorPessoa($id);
                $telefones = $comunicaTelefone->buscaPorPessoa($id);
                $enderecos = $comunicaEndereco->buscaPorIDPessoa($id);
                
                $registro = new Pessoas();
                $registro->setId($id);
                $registro->setNome($nome);
                $dtFormatada = $this->exibirData($data_nascimento);
                $registro->setDatanasc($dtFormatada);
                $registro->setSexo($sexo);
                $registro->setObs($obs);
                $registro->setCpfcnpj($cpf);
                $registro->setEmail($emails);
                $registro->setTelefones($telefones);
                $registro->setEnderecos($enderecos);
                               
                array_push($pessoas, $registro);
            }
            return $pessoas;
            /* close statement */
            $stmt->close();
        }
    }
	
	public function buscarPorTelefone($filtro){
        $pessoas= array();
        $comunicaEndereco = new EnderecoDAO($this->ParametroDb);
        $comunicaTelefone = new TelefoneDAO($this->ParametroDb);
        $comunicaEmail = new EmailDAO($this->ParametroDb);
        
        $query = "select pessoa.id, pessoa.nome, pessoa.data_nascimento, pessoa.sexo, pessoa.obs, pessoa.cpf from pessoa inner join telefone on pessoa.id = telefone.pessoa_id where telefone.telefone like '%$filtro%'";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $data_nascimento, $sexo, $obs, $cpf);
            /* fetch values */
            while ($stmt->fetch()) {
                
                
                $emails = $comunicaEmail->buscaPorPessoa($id);
                $telefones = $comunicaTelefone->buscaPorPessoa($id);
                $enderecos = $comunicaEndereco->buscaPorIDPessoa($id);
                
                $registro = new Pessoas();
                $registro->setId($id);
                $registro->setNome($nome);
                $dtFormatada = $this->exibirData($data_nascimento);
                $registro->setDatanasc($dtFormatada);
                $registro->setSexo($sexo);
                $registro->setObs($obs);
                $registro->setCpfcnpj($cpf);
                $registro->setEmail($emails);
                $registro->setTelefones($telefones);
                $registro->setEnderecos($enderecos);
                               
                array_push($pessoas, $registro);
            }
            return $pessoas;
            /* close statement */
            $stmt->close();
        }
    }
	
	public function buscarPorEmail($filtro){
        $pessoas= array();
        $comunicaEndereco = new EnderecoDAO($this->ParametroDb);
        $comunicaTelefone = new TelefoneDAO($this->ParametroDb);
        $comunicaEmail = new EmailDAO($this->ParametroDb);
        
        $query = "select pessoa.id, pessoa.nome, pessoa.data_nascimento, pessoa.sexo, pessoa.obs, pessoa.cpf from pessoa inner join email on pessoa.id = email.pessoa_id where email.email like '%$filtro%'";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $data_nascimento, $sexo, $obs, $cpf);
            /* fetch values */
            while ($stmt->fetch()) {
                
                
                $emails = $comunicaEmail->buscaPorPessoa($id);
                $telefones = $comunicaTelefone->buscaPorPessoa($id);
                $enderecos = $comunicaEndereco->buscaPorIDPessoa($id);
                
                $registro = new Pessoas();
                $registro->setId($id);
                $registro->setNome($nome);
                $dtFormatada = $this->exibirData($data_nascimento);
                $registro->setDatanasc($dtFormatada);
                $registro->setSexo($sexo);
                $registro->setObs($obs);
                $registro->setCpfcnpj($cpf);
                $registro->setEmail($emails);
                $registro->setTelefones($telefones);
                $registro->setEnderecos($enderecos);
                               
                array_push($pessoas, $registro);
            }
            return $pessoas;
            /* close statement */
            $stmt->close();
        }
    }
	
	public function buscarResponsavelPorNome($filtro, $ididoso){
        $pessoas= array();
        $idosoID = $ididoso;
		
        $query = "select id, nome, data_nascimento, sexo, obs, cpf from pessoa where nome like '%$filtro%' and pessoa.id not in (select pessoa_id from idoso) and pessoa.id not in (select pessoa_id from responsavel where idoso_id = ?)";
        if ($stmt = $this->mysqli->prepare($query)) {
			$stmt->bind_param('i', $idosoID);
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $data_nascimento, $sexo, $obs, $cpf);
            /* fetch values */
            while ($stmt->fetch()) {
                
                $registro = new Pessoas();
                $registro->setId($id);
                $registro->setNome($nome);
                $dtFormatada = $this->exibirData($data_nascimento);
                $registro->setDatanasc($dtFormatada);
                $registro->setSexo($sexo);
                $registro->setObs($obs);
                $registro->setCpfcnpj($cpf);
                               
                array_push($pessoas, $registro);
            }
            return $pessoas;
            /* close statement */
            $stmt->close();
        }
    }
	
}