<?php
class Email{
    private $id;
    private $email;
    private $idpessoa;
    
    
    public function getIdpessoa()
    {
        return $this->idpessoa;
    }
    public function setIdpessoa($idpessoa)
    {
        $this->idpessoa = $idpessoa;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function setEmail($email)
    {
        $this->email = $email;
    }
}