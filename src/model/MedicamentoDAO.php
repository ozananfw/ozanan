<?php
require_once ('../model/Medicamento.php');
require_once ('../model/ProdutosDAO.php');

class MedicamentoDAO
{
    private $mysqli;
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
    }
    
    public function buscaPorIdoso($ididoso){
        $medicamentos = array();
        $query = "select p.id, m.id as medicamento_id, p.nome, t.descricao, m.dosagem from produto p INNER JOIN medicamento m ON p.id = m.produto_id "
                . "INNER JOIN tipo_med t ON t.id = m.tipo_med_id inner join idoso_has_medicamento on medicamento_id = m.id "
				. "where idoso_id = ?";
        if ($stmt = $this->mysqli->prepare($query)) {
			$stmt->bind_param('i', $ididoso);
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $medicamento_id, $nome, $classe, $dosagem);
            /* fetch values */
            while ($stmt->fetch()) {
                $med = new Medicamento();
                $med->setId($id);
                $med->setNome($nome);
                $med->setClasse($classe);
                $med->setDosagem($dosagem);
				$med->setMedicamento_id($medicamento_id);
                array_push($medicamentos, $med);
            }
            return $medicamentos;
            /* close statement */
            $stmt->close();
        }
    }
	
	public function buscarMedicamentoPorNome($filtro, $ididoso){
        $medicamentos = array();
        $query = "select p.id, m.id as medicamento_id, p.nome, t.descricao, m.dosagem from produto p INNER JOIN medicamento m ON p.id = m.produto_id "
                . "INNER JOIN tipo_med t ON t.id = m.tipo_med_id "
				. "where m.id not in (select medicamento_id from idoso_has_medicamento where idoso_id = ?) and p.nome like '%$filtro%'";
        if ($stmt = $this->mysqli->prepare($query)) {
			$stmt->bind_param('i', $ididoso);
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $medicamento_id, $nome, $classe, $dosagem);
            /* fetch values */
            while ($stmt->fetch()) {
                $med = new Medicamento();
                $med->setId($id);
                $med->setNome($nome);
                $med->setClasse($classe);
                $med->setDosagem($dosagem);
				$med->setMedicamento_id($medicamento_id);
                array_push($medicamentos, $med);
            }
            return $medicamentos;
            /* close statement */
            $stmt->close();
        }
    }
	
    public function buscarTodos(){
        $medicamentos = array();
        $query = "select p.id, p.nome, t.descricao, m.dosagem from produto p INNER JOIN medicamento m ON p.id = m.produto_id "
                . "INNER JOIN tipo_med t ON t.id = m.tipo_med_id";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $classe, $dosagem);
            /* fetch values */
            while ($stmt->fetch()) {
                $med = new Medicamento();
                $med->setId($id);
                $med->setNome($nome);
                $med->setClasse($classe);
                $med->setDosagem($dosagem);
                array_push($medicamentos, $med);
            }
            return $medicamentos;
            /* close statement */
            $stmt->close();
        }
    }
    
    public function buscar($idmed){
        $query = "select p.id, p.nome, p.quantidade_minima, t.id, m.dosagem from produto p INNER JOIN medicamento m ON p.id = m.produto_id "
                . "INNER JOIN tipo_med t ON t.id = m.tipo_med_id where p.id = ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param('i', $idmed);
            $stmt->execute();
            $stmt->bind_result($id, $nome, $quantidade, $classe, $dosagem);
            while ($stmt->fetch()) {
                $med = new Medicamento();
                $med->setId($id);
                $med->setNome($nome);
                $med->setQuantidade_minima($quantidade);
                $med->setClasse($classe);   
                $med->setDosagem($dosagem);
            }
            return $med;
            $stmt->close();
        }
    }
    
    public function filtrar($busca, $campo){
        $query ="";
        if($campo == "Nome") {
            $query = "select p.id, p.nome, p.quantidade_minima, p.classificacao_id, t.descricao, m.dosagem from produto p INNER JOIN medicamento m ON p.id = m.produto_id "
                . "INNER JOIN tipo_med t ON t.id = m.tipo_med_id where p.nome = '$busca'";
        } else if($campo == "Classe") {
            $query = "select p.id, p.nome, p.quantidade_minima,p.classificacao_id, t.descricao, m.dosagem from produto p INNER JOIN medicamento m ON p.id = m.produto_id "
                . "INNER JOIN tipo_med t ON t.id = m.tipo_med_id where t.descricao = '$busca'";
        } else {
//            Terminar o filtro da dosagem, pois será necessário criar uma tabela no DB para ela, assim como o tipo_med
//            $query = "select p.id, p.nome, p.quantidade_minima, t.id, m.dosagem from produto p INNER JOIN medicamento m ON p.id = m.produto_id "
//                . "INNER JOIN tipo_med t ON t.id = m.tipo_med_id where m.dosagem = '$campo'";
        }
        if($query!= ""){
            if ($stmt = $this->mysqli->prepare($query)) {
                if($stmt->execute()){
//                    echo "<script>alert('executou')</script>";
                    $medicamento = array();
                    $stmt->bind_result($id, $nome, $quantidade_minima, $classificacao, $classe, $dosagem );
                    while ($stmt->fetch()) {
                        $med = new Medicamento();
                        $med->setId($id);
                        $med->setNome($nome);
                        $med->setClassificacao($classificacao);
                        $med->setQuantidade_minima($quantidade_minima);
                        $med->setClasse($classe);
                        $med->setDosagem($dosagem);
                        $medicamento[] = $med;    
                    }
                    return $medicamento;
                } else {
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
        $stmt->close();
    }
    
    public function remove($idmed){
        $query = "delete from medicamento where produto_id = ?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('i', $idmed);
        if($stmt->execute())
        {   $db = new Database();
            $produtoDAO = new ProdutosDAO($db);
            if($produtoDAO->remove($idmed)){
                return true;
            }
            return false;
        }else{
            return false;
        }
        $stmt->close();
        
    }
    
    public function atualiza($med){
        $db = new Database();
        $produto = new Produtos();
        $produto->setId($med->getId());
        $produto->setNome($med->getNome());
        $produto->setQuantidade_minima($med->getQuantidade_minima());
        $produto->setClassificacao($med->getClassificacao());
        $prodDAO = new ProdutosDAO($db);
        if($prodDAO->atualiza($produto)) {
            $query = "update medicamento set tipo_med_id = ?, dosagem = ? where produto_id = ?";
            $stmt = $this->mysqli->prepare($query);
            $idMedicamento = $med->getId();
            $tipo_med = $med->getClasse();
            $dosagem = $med->getDosagem();
            $stmt->bind_param('iii', $tipo_med, $dosagem, $idMedicamento);
            if($stmt->execute()) {
                return true;
            } else{
                return false;
            }
        } else {
            return false;
        }
        $stmt->close();
    }
    
    public function add(Medicamento $med, $id)
    {
        $dosagem = $med->getDosagem();
        $tipo_med = $med->getClasse();
            $query = "INSERT INTO medicamento (tipo_med_id, dosagem, produto_id) VALUES(?,?,?)";
            $stmt = $this->mysqli->prepare($query);
            $stmt->bind_param('iii', $tipo_med, $dosagem, $id);
            if($stmt->execute())
                return true;
            else
                return false;
        $stmt->close();
    }
    
}