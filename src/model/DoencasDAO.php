<?php
require_once ('../model/Doencas.php');

class DoencasDAO
{
    private $mysqli;
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
    }
    
    public function add(Doencas $doenca)
    {
        $descricao = $doenca->getDescricao();
        
        $query = "INSERT INTO doenca (doenca) VALUES(?)";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('s', $descricao);
        $stmt->execute();
        $stmt->close();
    }
    
    public function buscarTodos(){
        $doencas = array();
        $query = "select id, doenca from doenca";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $doenca);
            /* fetch values */
            while ($stmt->fetch()) {
                $registro = new Doencas();
                $registro->setId($id);
                $registro->setDescricao($doenca);
                
                array_push($doencas, $registro);
            }
            return $doencas;
            /* close statement */
            $stmt->close();
        }
    }
    
	
	public function buscarPorIdosso($id_idoso){
		$doencas = array();
		$query = "select if, doenca from doenca inner join doenca_has_idoso on doenca.id = doenca_has_idoso.doenca_id where idoso_id = ?";
		if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
			$stmt->bind_param('i', $id_idoso);
		
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $doenca);
            /* fetch values */
            while ($stmt->fetch()) {
                $registro = new Doencas();
                $registro->setId($id);
                $registro->setDescricao($doenca);
                
                array_push($doencas, $registro);
            }
            return $doencas;
            /* close statement */
            $stmt->close();
        }
	}
}