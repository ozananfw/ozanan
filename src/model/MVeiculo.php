<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 27/06/2018
 * Time: 01:46
 */
require_once "Pessoas.php";
require_once "Carro.php";
class MVeiculo
{
    private $id;
    private $pessoa;
    private $veiculo;
    private $kmSaida;
    private $kmEntrda;
    private $observacao;
    private $motivo;
    private $HSaida;
    private $Hentrada;
    private $Dsaida;
    private $Dentrada;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }

    /**
     * @param mixed $pessoa
     */
    public function setPessoa($pessoa)
    {
        $this->pessoa = $pessoa;
    }

    /**
     * @return mixed
     */
    public function getVeiculo()
    {
        return $this->veiculo;
    }

    /**
     * @param mixed $veiculo
     */
    public function setVeiculo($veiculo)
    {
        $this->veiculo = $veiculo;
    }

    /**
     * @return mixed
     */
    public function getKmSaida()
    {
        return $this->kmSaida;
    }

    /**
     * @param mixed $kmSaida
     */
    public function setKmSaida($kmSaida)
    {
        $this->kmSaida = $kmSaida;
    }

    /**
     * @return mixed
     */
    public function getKmEntrda()
    {
        return $this->kmEntrda;
    }

    /**
     * @param mixed $kmEntrda
     */
    public function setKmEntrda($kmEntrda)
    {
        $this->kmEntrda = $kmEntrda;
    }

    /**
     * @return mixed
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param mixed $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return mixed
     */
    public function getMotivo()
    {
        return $this->motivo;
    }

    /**
     * @param mixed $motivo
     */
    public function setMotivo($motivo)
    {
        $this->motivo = $motivo;
    }

    /**
     * @return mixed
     */
    public function getHSaida()
    {
        return $this->HSaida;
    }

    /**
     * @param mixed $HSaida
     */
    public function setHSaida($HSaida)
    {
        $this->HSaida = $HSaida;
    }

    /**
     * @return mixed
     */
    public function getHentrada()
    {
        return $this->Hentrada;
    }

    /**
     * @param mixed $Hentrada
     */
    public function setHentrada($Hentrada)
    {
        $this->Hentrada = $Hentrada;
    }

    /**
     * @return mixed
     */
    public function getDsaida()
    {
        return $this->Dsaida;
    }

    /**
     * @param mixed $Dsaida
     */
    public function setDsaida($Dsaida)
    {
        $this->Dsaida = $Dsaida;
    }

    /**
     * @return mixed
     */
    public function getDentrada()
    {
        return $this->Dentrada;
    }

    /**
     * @param mixed $Dentrada
     */
    public function setDentrada($Dentrada)
    {
        $this->Dentrada = $Dentrada;
    }





}