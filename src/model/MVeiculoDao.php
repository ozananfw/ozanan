<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 27/06/2018
 * Time: 01:46
 */
require_once "Pessoas.php";
require_once "Carro.php";
require_once "MVeiculo.php";
class MVeiculoDao
{
    private $mysqli;
    private $ParametroDb;


    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
        $this->ParametroDb = $db;
    }


    public function add(Pessoas $pessoa,Carro $carro,$kmSaida,$Hsaida,$motivo,$Dsaida,$obs)
    {
        $motorista = $pessoa->getId();
        $veiculo = $carro->getId();
        $query = "INSERT INTO movimentacao_carro(horario_saida, data_saida, motivo, km_saida, obs, funcionario_id, carro_id) VALUES (?, ?, ?, ?, ?, ?, ?);";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('sssisii', $Hsaida, $Dsaida,$motivo, $kmSaida, $obs, $motorista, $veiculo);

        if ($stmt->execute()){
            return true;
        }else{
            echo mysqli_error($this->mysqli);
        }
        $stmt->close();
    }

    public function buscar(){
        $query = "SELECT movimentacao_carro.id, pessoa.nome,carro.placa,movimentacao_carro.km_saida,movimentacao_carro.km_chegada, movimentacao_carro.data_saida, movimentacao_carro.data_chegada FROM pessoa JOIN funcionario ON pessoa.id = funcionario.pessoa_id JOIN movimentacao_carro ON funcionario.id = movimentacao_carro.funcionario_id JOIN carro ON carro.id = movimentacao_carro.carro_id";
        if ($stmt = $this->mysqli->prepare($query)) {
//            $stmt->bind_param('i', $id_funcionario);
            $stmt->execute();
            $stmt->bind_result($id, $nome, $placa, $kmSaida,$kmChegada,$dSaida,$dChegada);
            $mVeiculos = array();
            while ($stmt->fetch()) {
              $movimenta = new MVeiculo();
                $movimenta->setDentrada($dChegada);
                $movimenta->setDsaida($dSaida);
                $movimenta->setKmSaida($kmSaida);
                $movimenta->setKmEntrda($kmChegada);
                $movimenta->setPessoa($nome);
                $movimenta->setVeiculo($placa);
                $movimenta->setId($id);
                array_push($mVeiculos, $movimenta);
            }
            return $mVeiculos;
            $stmt->close();
        }
    }

    public function buscarUm($id){
        $query = "SELECT movimentacao_carro.id, pessoa.nome,carro.placa,movimentacao_carro.km_saida,movimentacao_carro.km_chegada, movimentacao_carro.data_saida, movimentacao_carro.data_chegada FROM pessoa JOIN funcionario ON pessoa.id = funcionario.pessoa_id JOIN movimentacao_carro ON funcionario.id = movimentacao_carro.funcionario_id JOIN carro ON carro.id = movimentacao_carro.carro_id WHERE movimentacao_carro.id = ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->bind_result($id, $nome, $placa, $kmSaida,$kmChegada,$dSaida,$dChegada);
            if($stmt->fetch()){
                $movimenta = new MVeiculo();
                $movimenta->setDentrada($dChegada);
                $movimenta->setDsaida($dSaida);
                $movimenta->setKmSaida($kmSaida);
                $movimenta->setKmEntrda($kmChegada);
                $movimenta->setPessoa($nome);
                $movimenta->setVeiculo($placa);
                $movimenta->setId($id);
                return $movimenta;
            }else{
                return false;
            }
            $stmt->close();
        }
    }

    public function altera(MVeiculo $veiculo)
    {
        $movimenta = $veiculo;
        $km = $movimenta->getKmEntrda();
        $mov = $movimenta->getDentrada();
        $id = $movimenta->getId();
        $query = "UPDATE movimentacao_carro SET km_chegada =? , data_chegada = ?  WHERE movimentacao_carro.id =?;";
        $stmt = $this->mysqli->prepare($query);
       $stmt->bind_param('isi', $km,$mov,$id);
        if($stmt->execute()){
            return true;
        }else{
            echo 'foi não';
        }
        $stmt->close();
    }




}