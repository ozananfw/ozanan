<?php
require_once ('../model/Classe.php');
class ClasseDAO
{
    private $mysqli;
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
    }
    public function buscarTodos(){
        $classes = array();
        $query = "select * from tipo_med";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $descricao);
            while ($stmt->fetch()) {
                $classe = new Classe();
                $classe->setId($id);
                $classe->setDescricao($descricao);
                array_push($classes, $classe);
            }
            return $classes;
            $stmt->close();
        }
    }
    public function buscar_id($id){
        $query = "select id, descricao from tipo_med where id = ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->bind_result($id, $descricao);
            $stmt->fetch();     
                $classe = new Classe();
                $classe->setId($id);
                $classe->setDescricao($descricao);
            
            return $classe;
            $stmt->close();
        }
    }
    
    public function add(Classe $classe) {
        $descricao = $classe->getDescricao();
        
        $query = "INSERT INTO tipo_med (descricao) VALUES(?)";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('s', $descricao);
        if($stmt->execute()) {
            return true;
        } else {
            return false;
        }
        $stmt->close();
    }
}