<?php
require_once ('../model/Produtos.php');
require_once ('../model/ProdutosDAO.php');
require_once ('../model/MovimentacaoHasProdutoDAO.php');
require_once ("../../funcoes.php");

class MovimentacaoDAO
{
    
    private $mysqli;
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
    }
    
    public function ultimaMovimentacao(){
        $id;
        $query = "select id from movimentacao order by id DESC limit 1";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id_resul);
            while ($stmt->fetch()) {
                $id = $id_resul;
            }
            return $id;
            $stmt->close();
        }
    }
        
    public function add($movimentacao) {
        $db = new Database();
        $responsavel = $movimentacao->getFuncionario_id();
        $solicitante = $movimentacao->getSolicitante_id();
        $data = $movimentacao->getData();
        $doador = $movimentacao->getDoador_id();
        if($doador == ""){
            $doador = NULL;
        }
        $origem = $movimentacao->getOrigem();
        $tipo = $movimentacao->getTipo();
        $query = "INSERT INTO movimentacao (tipo, data, origem, doador_id, funcionario_id, solicitante_id) VALUES(?,?,?,?,?,?)";
        if($stmt = $this->mysqli->prepare($query)){
            $stmt->bind_param("sssiii", $tipo, $data, $origem, $doador, $responsavel, $solicitante);
            $stmt->execute();
        }
        $stmt->close();
    }
    
    public function buscarTodos(){
        $movimentacoes = array();
        $db = new Database();
        $query = "SELECT * FROM movimentacao";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $tipo, $data, $origem, $doador_id, $funcionario_id, $solicitante_id);
            while ($stmt->fetch()) {
                $prod = new MovimentacaoHasProdutoDAO($db);
                $array_produtos= $prod->buscar($id);
                $movimentacao = new Movimentacao();
                $movimentacao->setId($id);
                $movimentacao->setProdutos($array_produtos);
                $movimentacao->setData($data);
                $movimentacao->setDoador_id($doador_id);
                $movimentacao->setFuncionario_id($funcionario_id);
                $movimentacao->setSolicitante_id($solicitante_id);
                $movimentacao->setOrigem($origem);
                $movimentacao->setTipo($tipo);
                array_push($movimentacoes, $movimentacao);
            }
            return $movimentacoes;
            $stmt->close();
        }
    }
    
    public function buscar($id){
        $db = new Database();
        $query = "SELECT * FROM movimentacao WHERE id = ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->bind_result($id, $tipo, $data, $origem, $doador_id, $funcionario_id, $solicitante_id);
            while ($stmt->fetch()) {
                $prod = new MovimentacaoHasProdutoDAO($db);
                $array_produtos= $prod->buscar($id);
                $movimentacao = new Movimentacao();
                $movimentacao->setId($id);
                $movimentacao->setProdutos($array_produtos);
                $movimentacao->setData($data);
                $movimentacao->setDoador_id($doador_id);
                $movimentacao->setFuncionario_id($funcionario_id);
                $movimentacao->setSolicitante_id($solicitante_id);
                $movimentacao->setOrigem($origem);
                $movimentacao->setTipo($tipo);
            }
            return $movimentacao;
            $stmt->close();
        }
    }
    
    public function atualiza($movimentacao){
        $db = new Database();
        $query = "update movimentacao set tipo = ?, data = ?, origem = ?, doador_id = ?, funcionario_id = ?, solicitante_id = ? where id = ?";
        if($stmt = $this->mysqli->prepare($query)){
            $responsavel = $movimentacao->getFuncionario_id();
            $solicitante = $movimentacao->getSolicitante_id();
            $data = $movimentacao->getData();
            $doador = $movimentacao->getDoador_id();
            if($doador == ""){
                $doador = NULL;
            }
            $origem = $movimentacao->getOrigem();
            $tipo = $movimentacao->getTipo();
            $id = $movimentacao->getId();
            $stmt->bind_param("sssiiii", $tipo, $data, $origem, $doador, $responsavel, $solicitante, $id);
            if($stmt->execute()) {
                return true;
            } else{
                return false;
            }
        }
        $stmt->close();
    }
    
    function remove($id) {
        $db = new Database();
        $prod = new MovimentacaoHasProdutoDAO($db);
        if($prod->remove($id)){
            $query = "delete from movimentacao where id = ?";
            $stmt = $this->mysqli->prepare($query);
            $stmt->bind_param('i', $id);
            if($stmt->execute()){  
                return true;
            } else {
                return false;
            }
            $stmt->close();
        }
    }
    
    function filtrar ($busca, $campo) {
        $query ="";
        if($campo == "solicitante") {
            $query = "SELECT m.* FROM movimentacao m INNER JOIN pessoa p ON p.id = m.solicitante_id WHERE p.nome = '$busca'";
        } else if($campo == "responsavel") {
            $query = "SELECT m.* FROM movimentacao m INNER JOIN login l ON l.id = m.funcionario_id INNER JOIN pessoa p "
                    . "ON p.id = l.pessoa_id WHERE p.nome = '$busca'";
        } else if($campo == "data") {
            $dataDB = dataDB($busca);
            $query = "SELECT * FROM movimentacao WHERE data = '$dataDB'";
        }
        if($query != ""){
            if ($stmt = $this->mysqli->prepare($query)) {
                if($stmt->execute()){
                    $movimentacao = array();
                    $stmt->bind_result($id, $tipo, $data, $origem, $doador_id, $funcionario_id, $solicitante_id );
                    while ($stmt->fetch()) {
                        $mov = new Movimentacao();
                        $mov->setId($id);
                        $mov->setTipo($tipo);
                        $mov->setData($data);
                        $mov->setOrigem($origem);
                        $mov->setDoador_id($doador_id);
                        $mov->setFuncionario_id($funcionario_id);
                        $mov->setSolicitante_id($solicitante_id);
                        $movimentacao[] = $mov;    
                    }
                    return $movimentacao;
                } else {
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
        $stmt->close();
    }
    
}