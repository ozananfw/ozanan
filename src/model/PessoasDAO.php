<?php
require_once ('../model/Pessoas.php');
require_once ('../model/EnderecoDAO.php');
require_once ('../model/EmailDAO.php');
require_once ('../model/TelefoneDAO.php');
require_once ('../model/Endereco.php');
require_once ('../model/Telefone.php');
require_once ('../model/Email.php');

class PessoasDAO{
    private $mysqli;
    private $ParametroDb;
    
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
        $this->ParametroDb = $db;
    }
    
    public function remove($pessoaID){
		$id = $pessoaID;
        $query = "DELETE FROM pessoa WHERE id = ?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->close();
    }
    
    public function ultimaPessoa(){
        $id;
        $query = "select id from pessoa order by id DESC limit 1";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id_resul);
            /* fetch values */
            while ($stmt->fetch()) {
                $id = $id_resul;
            }
            return $id;
            /* close statement */
            $stmt->close();
        }
    }
    function formatarData($data){
      $rData = implode("-", array_reverse(explode("/", trim($data))));
      return $rData;
	}
	
	function exibirData($data){
		$rData = explode("-", $data);
		$rDataFormatada = $rData[2].'/'.$rData[1].'/'.$rData[0];
		return $rDataFormatada;
	}
   
    public function add(Pessoas $pessoa)
    {        
        $nome= $pessoa->getNome();
        $cpfcnpj = $pessoa->getCpfcnpj();
		$datanasc = $pessoa->getDatanasc();
		$data = $this->formatarData($datanasc);
        $sexo = $pessoa->getSexo();
        $obs = $pessoa->getObs();
        $arrayEnderecos = $pessoa->getEnderecos();
        $arrayTelefones = $pessoa->getTelefones();
        $arrayEmail = $pessoa->getEmail();
        
        $query = "INSERT INTO pessoa (nome, data_nascimento, sexo, obs, cpf) VALUES(?,?,?,?,?)";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('sssss', $nome, $datanasc, $sexo, $obs, $cpfcnpj);
        $stmt->execute();
        $stmt->close();
        
    }
	
	public function altera(Pessoas $pessoa)
    {        
        $nome= $pessoa->getNome();
        $cpfcnpj = $pessoa->getCpfcnpj();
        $datanasc = $pessoa->getDatanasc();
		$data = $this->formatarData($datanasc);
        $sexo = $pessoa->getSexo();
        $obs = $pessoa->getObs();
        $id = $pessoa->getId();
        
        $query = "UPDATE pessoa SET nome=?, data_nascimento=?, sexo=?, obs=?, cpf=? WHERE id=?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('sssssi', $nome, $datanasc, $sexo, $obs, $cpfcnpj, $id);
        $stmt->execute();
        $stmt->close();
        
    }
    public function busca($pessoaID){
        $idRecebido = $pessoaID;
        
        $comunicaEndereco = new EnderecoDAO($this->ParametroDb);
        $comunicaTelefone = new TelefoneDAO($this->ParametroDb);
        $comunicaEmail = new EmailDAO($this->ParametroDb);
        
        $query = "select id, nome, data_nascimento, sexo, obs, cpf from pessoa where id = ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param('i', $idRecebido);
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $data_nascimento, $sexo, $obs, $cpf);
            /* fetch values */
            while ($stmt->fetch()) {
                
                $emails = $comunicaEmail->buscaPorPessoa($id);
                $telefones = $comunicaTelefone->buscaPorPessoa($id);
                $enderecos = $comunicaEndereco->buscaPorIDPessoa($id);
				
				
                $registro = new Pessoas();
                $registro->setId($id);
                $registro->setNome($nome);
				$dtFormatada = $this->exibirData($data_nascimento);
                $registro->setDatanasc($dtFormatada);
                $registro->setSexo($sexo);
                $registro->setObs($obs);
                $registro->setCpfcnpj($cpf);
                $registro->setEmail($emails);
                $registro->setTelefones($telefones);
                $registro->setEnderecos($enderecos);
                
                return $registro;
            }
            /* close statement */
            $stmt->close();
        }
    }
        
    public function buscarTodos(){
        $pessoas= array();
        $comunicaEndereco = new EnderecoDAO($this->ParametroDb);
        $comunicaTelefone = new TelefoneDAO($this->ParametroDb);
        $comunicaEmail = new EmailDAO($this->ParametroDb);
        
        $query = "select id, nome, data_nascimento, sexo, obs, cpf from pessoa where id not in (select pessoa_id from idoso)";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $data_nascimento, $sexo, $obs, $cpf);
            /* fetch values */
            while ($stmt->fetch()) {
                
                
                $emails = $comunicaEmail->buscaPorPessoa($id);
                $telefones = $comunicaTelefone->buscaPorPessoa($id);
                $enderecos = $comunicaEndereco->buscaPorIDPessoa($id);
                
                $registro = new Pessoas();
                $registro->setId($id);
                $registro->setNome($nome);
                $dtFormatada = $this->exibirData($data_nascimento);
                $registro->setDatanasc($dtFormatada);
                $registro->setSexo($sexo);
                $registro->setObs($obs);
                $registro->setCpfcnpj($cpf);
                $registro->setEmail($emails);
                $registro->setTelefones($telefones);
                $registro->setEnderecos($enderecos);
                               
                array_push($pessoas, $registro);
            }
            return $pessoas;
            /* close statement */
            $stmt->close();
        }
    }
	
	public function buscarPorNome($filtro){
        $pessoas= array();
        $comunicaEndereco = new EnderecoDAO($this->ParametroDb);
        $comunicaTelefone = new TelefoneDAO($this->ParametroDb);
        $comunicaEmail = new EmailDAO($this->ParametroDb);
        
        $query = "select id, nome, data_nascimento, sexo, obs, cpf from pessoa where nome like '%$filtro%' and id not in (select pessoa_id from idoso)";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $data_nascimento, $sexo, $obs, $cpf);
            /* fetch values */
            while ($stmt->fetch()) {
                
                
                $emails = $comunicaEmail->buscaPorPessoa($id);
                $telefones = $comunicaTelefone->buscaPorPessoa($id);
                $enderecos = $comunicaEndereco->buscaPorIDPessoa($id);
                
                $registro = new Pessoas();
                $registro->setId($id);
                $registro->setNome($nome);
                $dtFormatada = $this->exibirData($data_nascimento);
                $registro->setDatanasc($dtFormatada);
                $registro->setSexo($sexo);
                $registro->setObs($obs);
                $registro->setCpfcnpj($cpf);
                $registro->setEmail($emails);
                $registro->setTelefones($telefones);
                $registro->setEnderecos($enderecos);
                               
                array_push($pessoas, $registro);
            }
            return $pessoas;
            /* close statement */
            $stmt->close();
        }
    }
	
	public function buscarPorTelefone($filtro){
        $pessoas= array();
        $comunicaEndereco = new EnderecoDAO($this->ParametroDb);
        $comunicaTelefone = new TelefoneDAO($this->ParametroDb);
        $comunicaEmail = new EmailDAO($this->ParametroDb);
        
        $query = "select pessoa.id, pessoa.nome, pessoa.data_nascimento, pessoa.sexo, pessoa.obs, pessoa.cpf from pessoa inner join telefone on pessoa.id = telefone.pessoa_id where telefone.telefone like '%$filtro%' and id not in (select pessoa_id from idoso)";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $data_nascimento, $sexo, $obs, $cpf);
            /* fetch values */
            while ($stmt->fetch()) {
                
                
                $emails = $comunicaEmail->buscaPorPessoa($id);
                $telefones = $comunicaTelefone->buscaPorPessoa($id);
                $enderecos = $comunicaEndereco->buscaPorIDPessoa($id);
                
                $registro = new Pessoas();
                $registro->setId($id);
                $registro->setNome($nome);
                $dtFormatada = $this->exibirData($data_nascimento);
                $registro->setDatanasc($dtFormatada);
                $registro->setSexo($sexo);
                $registro->setObs($obs);
                $registro->setCpfcnpj($cpf);
                $registro->setEmail($emails);
                $registro->setTelefones($telefones);
                $registro->setEnderecos($enderecos);
                               
                array_push($pessoas, $registro);
            }
            return $pessoas;
            /* close statement */
            $stmt->close();
        }
    }
	
	public function buscarPorEmail($filtro){
        $pessoas= array();
        $comunicaEndereco = new EnderecoDAO($this->ParametroDb);
        $comunicaTelefone = new TelefoneDAO($this->ParametroDb);
        $comunicaEmail = new EmailDAO($this->ParametroDb);
        
        $query = "select pessoa.id, pessoa.nome, pessoa.data_nascimento, pessoa.sexo, pessoa.obs, pessoa.cpf from pessoa inner join email on pessoa.id = email.pessoa_id where email.email like '%$filtro%' and id not in (select pessoa_id from idoso)";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $data_nascimento, $sexo, $obs, $cpf);
            /* fetch values */
            while ($stmt->fetch()) {
                
                
                $emails = $comunicaEmail->buscaPorPessoa($id);
                $telefones = $comunicaTelefone->buscaPorPessoa($id);
                $enderecos = $comunicaEndereco->buscaPorIDPessoa($id);
                
                $registro = new Pessoas();
                $registro->setId($id);
                $registro->setNome($nome);
                $dtFormatada = $this->exibirData($data_nascimento);
                $registro->setDatanasc($dtFormatada);
                $registro->setSexo($sexo);
                $registro->setObs($obs);
                $registro->setCpfcnpj($cpf);
                $registro->setEmail($emails);
                $registro->setTelefones($telefones);
                $registro->setEnderecos($enderecos);
                               
                array_push($pessoas, $registro);
            }
            return $pessoas;
            /* close statement */
            $stmt->close();
        }
    }
}