<?php

require_once "Usuario.php";

class UsuarioDao {

    private $mysqli;

    public function __construct(Database $db) {
        $this->mysqli = $db->getConection();
    }

    public function add(Usuario $usuario) {

        $nome = $usuario->getUsuario();
        $senha = $usuario->getSenha();
        $id_pessoa = $usuario->getId_pessoa();

        $query = "INSERT INTO login(login, senha, pessoa_id) VALUES(?,?,?)";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param('ssi', $nome, $senha, $id_pessoa);
            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
        $stmt->close();
    }

    public function buscar($usuario, $senha) {
        $query = "SELECT id, login, pessoa_id FROM login WHERE login LIKE ? AND senha LIKE ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param('ss', $usuario, $senha);
            $stmt->execute();
            $stmt->bind_result($id, $login, $pessoa_id);
            if ($stmt->fetch()) {
                $usuario = new Usuario();
                $usuario->setId($id);
                $usuario->setUsuario($login);
                return $usuario;
            } else {
                return false;
            }
            $stmt->close();
        }
        return false;
    }

    public function verifica($usuario) {
        $query = "SELECT login FROM login WHERE senha LIKE ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param('s', $usuario);
            $stmt->execute();
            if ($stmt->fetch()) {
                return true;
            } else {
                return false;
            }
            $stmt->close();
        }
        return false;
    }




    public function buscarTodos() {
        $usuarios = array();
        $query = "SELECT login.id, pessoa.nome, login.login FROM pessoa JOIN login ON pessoa.id = login.pessoa_id";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $login);
            /* fetch values */
            while ($stmt->fetch()) {
                $usuario = new Usuario();
                $usuario->setId($id);
                $usuario->setUsuario($login);
                $usuario->setNome($nome);
                array_push($usuarios, $usuario);
            }
            return $usuarios;
            /* close statement */
            $stmt->close();
        }
    }

    public function remove($idusuario) {
        $query = "delete from login where id =?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('i', $idusuario);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
        $stmt->close();
    }

    public function atualiza($usuario) {
        $query = "update login set login=?, senha=? where id =?";
        $stmt = $this->mysqli->prepare($query);
     
        $id = $usuario->setId($_POST['id']);
        $login =  $usuario->setUsuario($_POST['usuario']);
        $senha = $usuario->setSenha($_POST['senha']);
     
        $stmt->bind_param('ssi', $login, $senha, $id);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
        $stmt->close();
    }

}
