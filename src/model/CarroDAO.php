<?php
require_once ('../model/Carro.php');
class CarroDAO
{
    private $mysqli;
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
    }
    
    //teste
    public function buscarTodos(){
        $carros = array();
        $query = "select id, nome, placa, status from carro";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $placa, $status);
            /* fetch values */
            while ($stmt->fetch()) {
                $carro = new Carro();
                $carro->setId($id);
                $carro->setNome($nome);
                $carro->setPlaca($placa);
                $carro->setStatus($status);
                
                array_push($carros, $carro);
            }
            return $carros;
            /* close statement */
            $stmt->close();
        }
    }
    
    public function buscar($idcarro){
        $query = "select id, nome, placa, status from carro where id=?";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param('i', $idcarro);
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $nome, $placa, $status);
            /* fetch values */
            while ($stmt->fetch()) {
                $carro = new Carro();
                $carro->setId($id);
                $carro->setNome($nome);
                $carro->setPlaca($placa);
                $carro->setStatus($status);
                
               
            }
            return $carro;
            /* close statement */
            $stmt->close();
        }
    }
    
    
    public function remove($idcarro){
        $query = "delete from carro where id =?";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('i', $idcarro);
        if($stmt->execute())
        {
            return true;
        }else{
            return false;
        }
        $stmt->close();
        
    }
    
    public function atualiza($carro){
        $query = "update carro set nome=?, placa=? where id =?";
        $stmt = $this->mysqli->prepare($query);
        $idCarro = $carro->getId();
        $nomeCarro = $carro->getNome();
        $placaCarro = $carro->getPlaca();
        $stmt->bind_param('ssi', $nomeCarro, $placaCarro, $idCarro);
        if($stmt->execute())
        {
            return true;
        }else{
            return false;
        }
        $stmt->close();    
    }
    
    public function add(Carro $carro)
    {
        $nome = $carro->getNome();
        $placa = $carro->getPlaca();
        $status = $carro->getStatus();
        
        $query = "INSERT INTO carro (nome, placa, status) VALUES(?,?,?)";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('sss', $nome, $placa, $status);
        if($stmt->execute())
            return true;
        else
            return false;
        $stmt->close();
    }
    
}