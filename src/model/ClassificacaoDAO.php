<?php
use mvc\Classificacao;
require_once ('../model/Classificacao.php');
class ClassificacaoDAO
{
    private $mysqli;
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
    }
    public function buscarTodos(){
        $classificacoes = array();
        $query = "select * from classificacao";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $descricao);
            while ($stmt->fetch()) {
                $classificacao = new Classificacao();
                $classificacao->setId($id);
                $classificacao->setDescricao($descricao);
                array_push($classificacoes, $classificacao);
            }
            return $classificacoes;
            $stmt->close();
        }
    }
    public function buscar_id($id){
        $query = "select id, descricao from classificacao where id = ?";
        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->bind_result($id, $descricao);
            $stmt->fetch();     
                $classificacao = new Classificacao();
                $classificacao->setId($id);
                $classificacao->setDescricao($descricao);
            
            return $classificacao;
            $stmt->close();
        }
    }
}