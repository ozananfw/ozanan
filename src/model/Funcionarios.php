<?php
require_once ('../model/Pessoas.php');
class Funcionarios{
    private $id;
    private $login;
    private $senha;
    private $pessoa;
    
    
    public function getId()
    {
        return $this->id;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getSenha()
    {
        return $this->senha;
    }

    public function getPessoa()
    {
        return $this->pessoa;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    public function setPessoa($pessoa)
    {
        $this->pessoa = $pessoa;
    }
}