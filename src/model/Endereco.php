<?php
class Endereco{
    private $id;
    private $endereco;
    private $numero;
    private $bairro;
    private $cidade;
    private $estado;
    private $cep;
    private $pessoaID;
    
    
    public function getPessoaID()
    {
        return $this->pessoaID;
    }

    public function setPessoaID($pessoaID)
    {
        $this->pessoaID = $pessoaID;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEndereco()
    {
        return $this->endereco;
    }

    public function getNumero()
    {
        return $this->numero;
    }

    public function getBairro()
    {
        return $this->bairro;
    }

    public function getCidade()
    {
        return $this->cidade;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function getCep()
    {
        return $this->cep;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function setCep($cep)
    {
        $this->cep = $cep;
    }
}
?>