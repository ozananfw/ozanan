<?php
require_once ('../model/Anotacoes.php');

class AnotacoesDAO
{
    private $mysqli;
    public function __construct(Database $db)
    {
        $this->mysqli = $db->getConection();
    }
    
    public function add(Anotacoes $anotacao)
    {
        $mensagem = $anotacao->getMensagem();
        $titulo = $anotacao->getTitulo();
        
        $query = "INSERT INTO anotacoes (mensagem, titulo) VALUES(?,?)";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param('ss', $mensagem, $titulo);
        $stmt->execute();
        $stmt->close();
    }
    
    public function buscarTodos(){
        $anotacoes = array();
        $query = "select id, mensagem, titulo from anotacoes";
        if ($stmt = $this->mysqli->prepare($query)) {
            /* execute statement */
            $stmt->execute();
            /* bind result variables */
            $stmt->bind_result($id, $mensagem, $titulo);
            /* fetch values */
            while ($stmt->fetch()) {
                $registro = new Anotacoes();
                $registro->setId($id);
                $registro->setMensagem($mensagem);
                $regsitro->setTitulo($titulo);
                
                array_push($anotacoes, $registro);
            }
            return $anotacoes;
            /* close statement */
            $stmt->close();
        }
    }
    
}