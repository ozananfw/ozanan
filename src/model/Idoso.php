<?php
class Idoso{
    private $id;
    private $nome;
    private $pessoa_id;
   
   
    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getPessoa_id()
    {
        return $this->pessoa_id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    public function setPessoa_id($pessoa_id)
    {
        $this->pessoa_id = $pessoa_id;
    }
    
}