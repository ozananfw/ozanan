<?php
include_once "../../funcoes.php";
require_once ('../model/Classificacao.php');
require_once ('../model/ClassificacaoDAO.php');
require_once ('../config/Database.php');
imprime_menu();
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<title>Cadastro Produtos </title>
	<link rel="stylesheet" href="../view/estilo.css">
</head>
<body>
<form method="post" action="../controller/ProdutosController.php?action=atualiza" id="dados" name="dados">
<div>

<h2>Cadastro de Produtos</h2>
<label for="descricao">Descrição</label>
 <input name="nome" value="<?=$produto->getNome();?>"></input>
 
 
 <label for="descricao">Quantidade mínima</label>
 <input style="width: 50px" type="number" name="quantidade_minima" name="quantidade_minima" value="<?=$produto->getQuantidade_minima();?>"></input>
 <input name="id" type="hidden" value=<?=$produto->getId();?>></input>
 
 <label for="descricao">Classificação</label>
 <select name="classificacao">
					<?php
					$db = new Database();
					$dao = new ClassificacaoDAO($db);
					$classificacoes=$dao->buscarTodos();
       if(isset($classificacoes) && !empty($classificacoes)) {
           echo "<option value=''>Selecione</option>";
            foreach($classificacoes as $classificacao) {
                $selected = $produto->getClassificacao() == $classificacao->getId() ? "selected" : "";
                echo "<option value={$classificacao->getId()} $selected>{$classificacao->getDescricao()}</option>";
            }
        } else {
            echo "<tr><td colspan=4 align='center'><font size=3>Não há classificacões cadastradas</font></td></tr>";
        }
        ?>  
        </select>
 
 <br></br>
		<button class="btnSalvar">Salvar</button>
		<button class="btnCancelar" type="button" name="voltar" id="voltar" onclick="window.location='../controller/ProdutosController.php?action=lista'">Cancelar</button>
</div>
</form>
</body>
</html>