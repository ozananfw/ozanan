<?php
include_once "../../funcoes.php";
require_once ('../config/Database.php');
require_once ('../model/Medicamento.php');
require_once ('../model/IdosoDAO.php');
require_once ('../model/medicamentoDAO.php');
imprime_menu();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Litagem de Medicamentos</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="javascriptmedicamento.js"></script>
  <link rel="stylesheet" href="../view/estiloResponsavel.css">
  
</head>
<body class="form_diferenciado">
<div class="container" id = "DIVcontainer">
  <h2>Litagem de Medicamentos</h2>
  <?php
	//session_start();
	$ididoso = $_SESSION['idosoID'];
	$db = new Database();
	$dao = new IdosoDAO($db);
	$pessoa = $dao->buscaPorIdoso($ididoso);
	echo "<h2>Paciente: {$pessoa->getNome()}</h2>";
	
?>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Adicionar Medicamento</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Adicionar Medicamento</h4>
        </div>
        <div class="modal-body">
		<form method="POST" id="form-pesquisa" action="">
			Nome: <input type="text" name="pesquisaMed" id="pesquisaMed" placeholder="O que você está procurando?">
		</form>
		
		<ul class="resultado">
		
		</ul>
		</table>
		
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>    
    </div>
  </div>
  
  	<div class='divtabela'>
	 <table id = 'dsTable'>
	<thead>
        <tr style="background: none">
		<th>Medicamento</th>
		<th>Dosagem</th>
		<th width="30">Ações</th>
		</tr>
	</thead>
	<?php   
	$db = new Database();
	$dao = new MedicamentoDAO($db);
	$ididoso = $_SESSION['idosoID'];
	$medicamentos = $dao->buscaPorIdoso($ididoso);
	
    if(isset($medicamentos) && !empty($medicamentos)) {
        foreach($medicamentos as $medicamento) {
		$db = new Database();
        echo "<tr id=tr_{$medicamento->getId()}>
              <td>{$medicamento->getNome()}</td>
			  <td>{$medicamento->getDosagem()}</td>
              <td><a href='../controller/IdosoController.php?action=removeMedicamento&ididoso={$ididoso}&idmedicamento={$medicamento->getMedicamento_id()}' title='Remover Medicamento'><img src='../view/imagens/lixeira24.png' /></a>
              </td>
              </tr>";
        }
    } else {
      echo "<tr><td colspan=4 align='center'><font size=3>Não há medicamentos cadastrados</font></td></tr>";
    }
    ?>
	</div>
</div>
</body>
</html>