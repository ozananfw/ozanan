<?php
include_once "../../funcoes.php";
require_once ('../model/Medicamento.php');
require_once ('../model/Classe.php');
require_once ('../model/MedicamentoDAO.php');
require_once ('../model/ClasseDAO.php');
require_once ('../config/Database.php');
imprime_menu();
$db = new Database();
$daoTipoMedicamento = new ClasseDAO($db);
$tipo_med = $daoTipoMedicamento->buscarTodos();
?>
<script> 
function adicionaClasse(){
        $('#dialog').dialog('option', 'width', '400');
        $('#dialog').dialog('option', 'height', '200');
        $('#dialog').dialog('option', 'background', '#e6e6e6');
        $('#dialog').html("Nome: <input type='text' name='descricao_classe' id='descricao_classe' style='width: 70%;'><br>");
        $('#dialog').dialog('option', 'buttons', {
            'Salvar': function() {
                $.post('../controller/ClasseController.php?action=adiciona',{descricao:$("#descricao_classe").val()},
                    function(data) {
                    if(data){
                        alert('Cadastro realizado com sucesso!');
                        $.get('../controller/ClasseController.php?action=lista', function(dados){
                            $("#classe").html(dados);
                        });
                        $('#dialog').dialog('close');
                    } else {
                        alert("Algum problema ocorreu. Recarregue a página novamente");
                    }
                });
            },
            'Fechar': function() {
                $('#dialog').dialog('close');
            }
        });
        $('#dialog').dialog('open');
    }
</script>
<html>
    <head>
        <meta charset="UTF-8">
		<link rel="stylesheet" href="../view/estilo_teste.css">
        <title>Cadastro de Medicamentos</title>
    </head>
    <body>
        <form action="../controller/MedicamentoController.php?action=atualiza" method="POST" id="dados" name="dados" class="form">
            <div class="div">
                <h2>
                        Cadastro de Medicamentos
                </h2>
                <input type="hidden" value="<?=$medicamento->getId()?>" name="id" id="id">
                <label>Nome</label><input type="text" name="nome" id="nome" required="" value="<?=$medicamento->getNome()?>">
                <label>Quantidade mínima</label><input type="text" name="quantidade_minima" id="quantidade_minima" value="<?=$medicamento->getQuantidade_minima()?>">
                <label><img src='../view/imagens/mais24.png' onclick="javascript:adicionaClasse();" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Classe</label>
                <select name="classe" id="classe" required="">
                    <option value="">Selecione...</option>
                    <?php
                    foreach ($tipo_med as $tipo) {
                        $selected = "";
                        if($medicamento->getClasse() == $tipo->getId()){
                            $selected = "selected";
                        }
                        echo "<option value='{$tipo->getId()}' $selected>".utf8_encode($tipo->getDescricao())."</option>";
                    }
                    ?>
                </select>
                <label>Forma Farmacêutica</label>
                <select name="dosagem" id="dosagem" required="">
                        <option value="">Selecione</option>
                        <option value="1" <?=$medicamento->getDosagem() == 1 ? "selected" : ""?>>Gotas</option>
                        <option value="2" <?=$medicamento->getDosagem() == 2 ? "selected" : ""?>>Compromido</option>
                        <option value="3" <?=$medicamento->getDosagem() == 3 ? "selected" : ""?>>Em pó</option>
                </select>
                <br>
                <button type="submit" class="btnSalvar" name="salvar" id="salvar">Salvar</button>
                <button class="btnCancelar" type="button" name="voltar" id="voltar" onclick="window.location='../controller/MedicamentoController.php?action=lista'">Cancelar</button>
            </div>
        </form>
    </body>
</html>