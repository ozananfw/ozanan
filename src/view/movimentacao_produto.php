<?php 
include_once "../../funcoes.php"; 
require_once ('../model/Produtos.php'); 
require_once ('../model/ProdutosDAO.php'); 
require_once ('../config/Database.php'); 
require_once ('../model/Usuario.php');
require_once ('../model/UsuarioDAO.php');
require_once ('../model/Pessoas.php');
require_once ('../model/PessoasDAO.php');
imprime_menu(); 
$db = new Database(); 
$dao = new ProdutosDAO($db); 
$produtos=$dao->buscarTodos();
$dao = new PessoasDAO($db);
$pessoas=$dao->buscarTodos();
?> 

<html lang="pt-br"> 
<head> 
<script type="text/javascript"> 
        function adiciona(){ 
        var div = document.getElementById("tabela_produtos"); 
        div.innerHTML += ` 
            <tr><td  width="65%"><select required="" id="produto[]" name="produto[]" class='selectGrande'> 
          <?php  
            echo "<option value='' >Selecione</option>"; 
             foreach($produtos as $produto) { 
                 echo "<option value={$produto->getId()}>{$produto->getNome()}</option>"; 
             } 
         ?>   
         </select></td>

       <td><input id="quantidade[]" name="quantidade[]" type="text" maxlength="5" size="30" class='inputPequeno'></td></tr>
       ` 
       } 
    </script>    
<title>Cadastro Movimentação </title> 
 <link rel="stylesheet" href="estilo_movimentacao_produto.css"> 
</head>       
  
<body>
	<form method="post" action="../controller/MovimentacaoController.php?action=adiciona" id="dados" name="dados">
	<div>
	<h2>Movimentação de Produtos</h2> 
        <label>Responsável: <?= strtoupper($_SESSION["usuario"])?></label><br><br>
        <label>Solicitante</label> 
        <select id="solicitante" name="solicitante" class="selectMedio">
            <?php
            echo "<option value=''>Selecione</option>";
            foreach($pessoas as $pessoa) {
                echo "<option value={$pessoa->getId()}>{$pessoa->getNome()}</option>";
            }
            ?>  
        </select>
        <br>
        <label>Data</label> 
        <input type="date" name="data" id="data" style="width: 150px" value="<?=date('Y-m-d')?>" > 
        <input type="hidden" value="<?=$_SESSION["id_usuario"]?>" name="id_usuario" id="id_usuario">
        <label class="novoLabel">Tipo</label> 
        <select required id="tipo" name="tipo" class="selectPequeno"> 
            <option value="">Selecione</option> 
            <option value="E">Entrada</option> 
            <option value="S">Saída</option> 
        </select> 
        <br>
         <label id="origem1" style="display: none" >Origem </label> 
        <select id="origem" name="origem" style="display: none" class="selectPequeno"> 
            <option value="">Selecione</option> 
            <option value="D">Doação</option> 
            <option value="C">Compra</option>
        </select>
         <label id="doador" style= "display:none" >Doador </label> 
        <select id="doadores" name="doadores" style="display: none" class="selectPequeno">
            <?php
            echo "<option value=''>Selecione</option>";
            foreach($pessoas as $pessoa) {
                echo "<option value={$pessoa->getId()}>{$pessoa->getNome()}</option>";
            }
            ?>  
        </select>
         <br><br>
         <table id="tabela_produtos">
             <tr>
                 <td width="65%" align="center"><b>PRODUTOS</b></td>
                 <td align="left"><b>QUANTIDADE</b></td>
                 <td align="left"><a href='#' onClick='adiciona()'><img src='../view/imagens/mais24.png' /></a></td>
             </tr>
             <tr>
                 <td  width="65%">
                    <select required id="produto[]" name="produto[]" class="selectGrande">
                        <?php
                        echo "<option value=''>Selecione</option>";
                        foreach($produtos as $produto) {
                             echo "<option value={$produto->getId()}>{$produto->getNome()}</option>";
                        }
                         ?>  
                    </select>  
                 </td>
                 <td>
                     <input id="quantidade[]" name="quantidade[]" type="text" maxlength="5" size="30" class="inputPequeno">
                 </td>
             </tr>
         </table>
        <br><br>
        
        <button class="btnSalvar">Salvar</button>
        <button class="btnCancelar" type="button" name="voltar" id="voltar" onclick="window.location='../controller/MovimentacaoController.php?action=lista'">Cancelar</button>
        <br><br>
        </div>
    </form>
</body> 
</html> 
<script> 
    $(document).ready(function (){ 
        $("#tipo").change(function(){ 
            if($("#tipo").val() == "E"){ 
                $("#origem").show(); 
                $("#origem1").show();
            } else { 
                $("#origem").hide(); 
                $("#origem1").hide(); 
            } 
        }); 
        $("#origem").change(function(){ 
            if($("#origem").val() == "D"){ 
                $("#doador").show(); 
                $("#doadores").show();
            } else { 
                $("#doador").hide(); 
                $("#doadores").hide(); 
            } 
        }); 
    }); 
</script>

