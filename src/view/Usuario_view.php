<!DOCTYPE html>
<?php
include_once "../../funcoes.php";
require_once ('../model/Pessoas.php');
require_once "../model/PessoasDao.php";
require_once ('../config/Database.php');
imprime_menu();
?>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>

    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="../view/estilo.css">
        <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>﻿
        <style>
            .divEsquerda {
                padding: 0px;
                float: left;
                width: 49%;
                margin: auto;
                border-radius: 0px;
                border-right: solid 1px black;

            }

            .divDireita {
                padding: 0px;
                float: left;
                width: 50%;
                margin: auto;
            }

            form {
                margin: auto;
                padding: 0px;
                width: 90%;
            }

            .divForm {
                width: 90%;
                padding: 0px;
            }


            .divTable {
                width: 80%;
            }

            tbody {
                overflow-y: scroll;
                overflow-x: hidden;
            }

            table{
                overflow-x: scroll;
            }


        </style>

        <script>
            function deleteRow(row, usuario) {
alert(usuario);
                if (confirm("Tem certeza que deseja apagar?")) {
                    $.post('../controller/UsuarioController.php?action=remove', {postusuario: usuario},
                            function (data)
                            {
                                if (data) {
                                    //apaga a linha da tabela em html
                                    row.parentNode.parentNode.parentNode.parentNode.deleteRow(row.parentNode.parentNode.rowIndex);
                                } else {
                                    alert("Algum problema ocorreu. Recarregue a página novamente");
                                }
                            });
                }

            }

            function esconde_conteduo(params) {
                
                document.getElementsByClassName('divEsquerda').script;

            }

        </script>        

    </head>

    <body>
        <div class="divEsquerda">
            <form method="POST" action="../controller/UsuarioController.php?action=adiciona" id="dados" name="dados">
                <div style="width: 80%; padding: 0px; margin: auto">
                    <h2>Cadastro de Usuarios</h2>
                    <fieldset>
                        <legend>Dados Pessoais</legend>

                        <div style="margin: auto; width: 50%">
                            <label>Pessoa</label>
                            <select name="idpessoa">
                                <option value="null">Escolha Uma Pessoa</option>

                                <?php
                                $db = new Database();
                                $dao = new PessoasDAO($db);
                                $pessoas = $dao->buscarTodos();

                                foreach ($pessoas as $key => $value) {
                                    $nome = $value->getNome();
                                    $id = $value->getId();

                                    echo '<option value=' . $id . '>';
                                    echo $nome;
                                    echo '</option>';
                                }
                                ?>
                            </select>
                            <label for="usuario">Usuario</label>
                            <input id="usuario" name="usuario" type="text" placeholder="Usuario. ex: bduarte" maxlength="7" size="7" required />
                            <label>Senha</label>
                            <input id="senha" name="senha" type="password" placeholder="***" maxlength="8" size="8" />
                        </div>


                    </fieldset>

                    <button type="button" class="btnCancelar">Cancelar</button>
                    <button type="submit" class="btnSalvar">Salvar</button>
                </div>
            </form>
        </div>


        <div class="divDireita">

            <table id='dsTable' style="margin: auto">
                <thead>
                    <tr style="background: none">
                        <th>Nome</th>
                        <th>Usuario</th>
                        <th width="60">Ações</th>
                    </tr>
                </thead>


                <?php
                foreach ($usuarios as $key => $usuario) {
                    echo " <tr>
                    <td>" . $usuario->getNome() . "</td>
                    <td>" . $usuario->getUsuario() . "</td>
                    <td>
                        <a href='#' onClick='deleteRow(this, {$usuario->getId()})'>
                            <img src='../view/imagens/lixeira24.png' />
                        </a>
                        <a href='../controller/UsuarioController.php?action=edita&idusuario={$usuario->getId()}'>
                            <img src='../view/imagens/lapis24.png' />
                        </a>
                    </td>
                </tr>";
                }
                ?>

















            </table>


        </div>


        <script>

            $('table').on('scroll', function () {
                $("table > *").width($("table").width() + $("table").scrollLeft());
            });

        </script>

    </body>

</html>