<?php
include_once "../../funcoes.php";
require_once ('../model/Pessoas.php');
require_once "../model/PessoasDao.php";
require_once ('../config/Database.php');
imprime_menu();
?>


<html lang="pt-br">



    <head>
        <meta charset="utf-8" />
        <title>Cadastro de Usuario</title>
        <link rel="stylesheet" href="estilo.css">

    </head>

    <body>
        <form method="POST" action="../controller/UsuarioController.php?action=adiciona" id="dados" name="dados">
            <div>
                <h2>Cadastro de Usuarios</h2>
                <fieldset>
                    <legend>Dados Pessoais</legend>

                    <div style="margin: auto; width: 50%">
                        <label>Pessoa</label>
                        <select name="idpessoa">
                            <option value="null">Escolha Uma Pessoa</option>

<?php
$db = new Database();
$dao = new PessoasDAO($db);
$pessoas = $dao->buscarTodos();

foreach ($pessoas as $key => $value) {
    $nome = $value->getNome();
    $id = $value->getId();

    echo '<option value=' . $id . '>';
    echo $nome;
    echo '</option>';
}
?>
                        </select>
                        <label for="usuario">Usuario</label>
                        <input id="usuario" name="usuario" type="text" placeholder="Usuario. ex: bduarte" maxlength="7" size="7" required />
                        <label>Senha</label>
                        <input id="senha" name="senha" type="password" placeholder="***" maxlength="8" size="8" />
                    </div>


                </fieldset>

                <button type="button" class="btnCancelar">Cancelar</button>
                <button type="submit" class="btnSalvar">Salvar</button>
            </div>
        </form>
    </body>

</html>