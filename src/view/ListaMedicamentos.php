<?php
include_once "../../funcoes.php";
imprime_menu();
?>
<html>
<head>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>﻿
<script type="text/javascript">
function deleteRow(row,idmed){

	if(confirm("Tem certeza que deseja apagar?")){
	$.post('../controller/MedicamentoController.php?action=remove',{postmed:idmed},
            function(data) {
            if(data){
            //apaga a linha da tabela em html
            row.parentNode.parentNode.parentNode.parentNode.deleteRow(row.parentNode.parentNode.rowIndex);
            }else{
                alert("Algum problema ocorreu. Recarregue a página novamente");
                }
            });
	}

}
</script>  
<meta>
<title>Medicamentos</title>
<link rel="stylesheet" href="../view/estilo.css">
</head>
<body>
    <form class="form" name="formmed" id="formmed" action="../controller/MedicamentoController.php?action=filtrar" method="POST">
		<h2>Listagem de Medicamentos</h2>
                <select class="selectFiltro" id="campo" name="campo" onchange="changeSelect()" required="">
			<option value="">Campo:</option>
			<option value="Nome">Nome</option>
			<option value="Classe">Classe</option>
		</select>
                <input class="campoFiltro" type="text" name="busca" placeholder="Buscar..." required="">
                <button class="btnBuscar" onclick="">Buscar</button>
		<table id = 'dsTable'>
			<thead>
                            <tr style="background: none">
				<th>Nome</th>
				<th>Classe</th> 
				<th>Forma Farmacêutica</th>
				<th width="60">Ações</th>
				</tr>
			</thead>
			 <?php   
       if(isset($medicamentos)) {
            foreach($medicamentos as $med) {
            $dosagem = $med->getDosagem() == 1 ? 'Gotas' : $med->getDosagem() == 2 ? 'Comprimido' : 'Em pó';
            echo "<tr id=tr_{$med->getId()}>
                <td>{$med->getNome()}</td>
                <td>". utf8_encode($med->getClasse())."</td>
                <td width=20%>$dosagem</td>
                <td><a href='#' onClick='deleteRow(this,{$med->getId()})'><img src='../view/imagens/lixeira24.png' /></a>
                <a href='../controller/MedicamentoController.php?action=edita&idmed={$med->getId()}'><img src='../view/imagens/lapis24.png' /></a>
                </td>
                </tr>";
            }
        } else {
            echo "<tr><td colspan=4 align='center'><font size=3>Não há medicamentos cadastrados</font></td></tr>";
        }
        ?> 
			
		</table>
	</form>

</body>
</html>