<?php
	//Incluir a conexão com banco de dados
	include_once('conexao.php');
	require_once ('../config/Database.php');
	require_once ('../model/Pessoas.php');
	require_once ('../model/PessoasDAO.php');
	
	//Recuperar o valor da palavra
	$cursos = $_POST['palavra'];
	$db = new Database();
    $dao = new PessoasDAO($db);
	
	//Pesquisar no banco de dados nome do curso referente a palavra digitada pelo usuário
	//$cursos = "SELECT * FROM pessoas WHERE nome LIKE '%$cursos%'";
	$pessoas = $dao->buscarPorNome($cursos);
	//$resultado_cursos = mysqli_query($conn, $cursos);
	
	
	echo "<table class='table' id = 'dsTable'>";
	echo "<thead>";
	echo "<tr style='background: none'>";
	echo "<th>Nome</th>";
	echo "</tr>";
	echo "</thead>";
	
	if(isset($pessoas) && !empty($pessoas)) {
		foreach($pessoas as $pessoa) {
		echo "<tr id=tr_{$pessoa->getId()}>
		<td>{$pessoa->getNome()}</td>
		</tr>";
		}
	} else {
		echo "<tr><td colspan=4 align='center'><font size=3>Não há pessoas cadastrados</font></td></tr>";
	}
?>