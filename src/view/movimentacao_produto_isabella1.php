<?php 
include_once "../../funcoes.php"; 
require_once ('../model/Produtos.php'); 
require_once ('../model/ProdutosDAO.php'); 
require_once ('../config/Database.php'); 
require_once ('../model/Usuario.php');
require_once ('../model/UsuarioDAO.php');
require_once ('../model/Pessoas.php');
require_once ('../model/PessoasDAO.php');
imprime_menu(); 
$db = new Database(); 
$dao = new ProdutosDAO($db); 
$produtos=$dao->buscarTodos();
$dao = new PessoasDAO($db);
$pessoas=$dao->buscarTodos();
?> 

<html lang="pt-br"> 
<head>  
<script type="text/javascript"> 
        function adiciona(){ 
        var div = document.getElementById("insere"); 
        div.innerHTML += ` 
            <label>Produtos</label><select required="" id="produto[]" name="produto[]"> 
          <?php  
            echo "<option value='' >Selecione</option>"; 
             foreach($produtos as $produto) { 
                 echo "<option value={$produto->getId()}>{$produto->getNome()}</option>"; 
             } 
         ?>   
         </select> 

       <label>Quantidade</label><input id="quantidade[]" name="quantidade[]" type="text" placeholder="Digite a quantidade " maxlength="5" size="30"/input>  <section id="insere" style="background: none"> 
       ` 
       } 
    </script>    
<title>Cadastro Movimentação </title> 
  <link rel="stylesheet" href="estilo.css"> 
</head> 

<body>
	<form method="post" action="../controller/MovimentacaoController.php?action=adiciona" id="dados" name="dados">
	<div>
	<h2>Movimentação de Produtos</h2> 
    
        <label>Solicitante</label> 
        <select id="solicitante" name="solicitante">
            <?php
            echo "<option value=''>Selecione</option>";
            foreach($pessoas as $pessoa) {
                echo "<option value={$pessoa->getId()}>{$pessoa->getNome()}</option>";
            }
            ?>  
        </select>

        <label>Data</label> 
        <input type="date" name="data" id="data" style="width: 150px" value="<?=date('Y-m-d')?>"> 
        <input type="hidden" value="<?=$_SESSION["id_usuario"]?>" name="id_usuario" id="id_usuario">
        <label>Tipo</label> 
        <select required id="tipo" name="tipo"> 
            <option value="">Selecione</option> 
            <option value="E">Entrada</option> 
            <option value="S">Saída</option> 
        </select> 
         <label id="origem1" style="display: none">Origem </label> 
        <select id="origem" name="origem" style="display: none"> 
            <option value="">Selecione</option> 
            <option value="D">Doação</option> 
            <option value="C">Compra</option>
        </select>
        <label id="doador" style= "display:none">Doador </label> 
        <select id="doadores" name="doadores" style="display: none">
            <?php
            echo "<option value=''>Selecione</option>";
            foreach($pessoas as $pessoa) {
                echo "<option value={$pessoa->getId()}>{$pessoa->getNome()}</option>";
            }
            ?>  
        </select>

        <label>Produtos</label>
        <select required id="produto[]" name="produto[]">
            <?php
            echo "<option value=''>Selecione</option>";
            foreach($produtos as $produto) {
                 echo "<option value={$produto->getId()}>{$produto->getNome()}</option>";
            }
             ?>  
        </select>

        <label>Quantidade</label>
        <input id="quantidade[]" name="quantidade[]" type="text" placeholder="Digite a quantidade " maxlength="5" size="30">
        	
        <section id="insere" style="background: none"></section>
        <a href='#' onClick='adiciona()'><img src='../view/imagens/mais24.png' /></a>
        <br><br>

        <button class="btnCancelar" type="button" name="voltar" id="voltar" onclick="window.location='../controller/MovimentacaoController.php?action=lista'">Cancelar</button>
  		<button class="btnSalvar">Salvar</button>
  		</div>
    </form>
</body> 
</html> 
<script> 
    $(document).ready(function (){ 
        $("#tipo").change(function(){ 
            if($("#tipo").val() == "E"){ 
                $("#origem").show(); 
                $("#origem1").show();
            } else { 
                $("#origem").hide(); 
                $("#origem1").hide(); 
            } 
        }); 
        $("#origem").change(function(){ 
            if($("#origem").val() == "D"){ 
                $("#doador").show(); 
                $("#doadores").show();
            } else { 
                $("#doador").hide(); 
                $("#doadores").hide(); 
            } 
        }); 
    }); 
</script>

