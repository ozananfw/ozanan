<?php
	//Incluir a conexão com banco de dados
	//include_once('conexao.php');
	require_once ('../config/Database.php');
	require_once ('../model/Pessoas.php');
	require_once ('../model/IdosoDAO.php');
	
	//Recuperar o valor da palavra
	$busca = $_POST['palavra'];
	$db = new Database();
    $dao = new IdosoDAO($db);
	
	session_start();
	$ididoso = $_SESSION['idosoID'];
	
	//Pesquisar no banco de dados nome do curso referente a palavra digitada pelo usuário
	$pessoas = $dao->buscarResponsavelPorNome($busca, $ididoso);
	
	
	echo "<table class='table' id = 'dsTable'>";
	echo "<thead>";
	echo "<tr style='background: none'>";
	echo "<th>Nome</th>";
	echo "<th width='30'>Ações</th>";
	echo "</tr>";
	echo "</thead>";
	
	if(isset($pessoas) && !empty($pessoas)) {
		foreach($pessoas as $pessoa) {
		echo "<tr id=tr_{$pessoa->getId()}>
		<td>{$pessoa->getNome()}</td>
		<td><a href='../controller/IdosoController.php?action=addResponsavel&ididoso={$ididoso}&idresponsavel={$pessoa->getId()}' title='Adicionar Responsável'><img src='../view/imagens/checked.png' /></a></td>
		</tr>";
		}
	} else {
		echo "<tr><td colspan=4 align='center'><font size=3>Não há pessoas cadastrados</font></td></tr>";
	}
?>