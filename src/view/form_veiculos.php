<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Cadastro Veículo </title>
	<link rel="stylesheet" href="estilo.css">
</head>

<style>

</style>

<body>
<?php include_once "../../funcoes.php";
imprime_menu();
?>

    <form method="post" action="../controller/CarroController.php?action=adiciona" id="dados" name="dados">
        <div>
            <h2>Cadastro de Veículos</h2>
            <label for="descricao">Modelo</label>
            <input placeholder="Nome do veículo" name="nome" required=""></input>
            <label for="descricao">Placa</label>
            <input placeholder="placa" name="placa" required=""></input>
            <button type="submit" class="btnSalvar">Salvar</button>
            <button type=button class="btnCancelar" onclick="window.location='../view/ListaCarros.php'">Cancelar</button>
            </p>
        </div>
    </form>
</body>
</html>