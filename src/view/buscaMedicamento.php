<?php
	//Incluir a conexão com banco de dados
	//include_once('conexao.php');
	require_once ('../config/Database.php');
	require_once ('../model/MedicamentoDAO.php');
	require_once ('../model/Medicamento.php');
	
	//Recuperar o valor da palavra
	$busca = $_POST['palavra'];
	$db = new Database();
    $dao = new MedicamentoDAO($db);
	
	session_start();
	$ididoso = $_SESSION['idosoID'];
	
	//Pesquisar no banco de dados nome do curso referente a palavra digitada pelo usuário
	$medicamentos = $dao->buscarMedicamentoPorNome($busca, $ididoso);
	
	
	echo "<table class='table' id = 'dsTable'>";
	echo "<thead>";
	echo "<tr style='background: none'>";
	echo "<th>Medicamento</th>";
	echo "<th>Dosagem</th>";
	echo "<th width='30'>Ações</th>";
	echo "</tr>";
	echo "</thead>";
	
	if(isset($medicamentos) && !empty($medicamentos)) {
		foreach($medicamentos as $medicamento) {
		echo "<tr id=tr_{$medicamento->getId()}>
		<td>{$medicamento->getNome()}</td>
		<td>{$medicamento->getDosagem()}</td>
		<td><a href='../controller/IdosoController.php?action=addMedicamento&ididoso={$ididoso}&idmedicamento={$medicamento->getMedicamento_id()}' title='Adicionar Medicamento'><img src='../view/imagens/checked.png' /></a></td>
		</tr>";
		}
	} else {
		echo "<tr><td colspan=4 align='center'><font size=3>Não há medicamento cadastrado</font></td></tr>";
	}
?>