<?php 
include_once "../../funcoes.php"; 
require_once ('../model/Produtos.php'); 
require_once ('../model/ProdutosDAO.php'); 
require_once ('../config/Database.php'); 
require_once ('../model/Usuario.php');
require_once ('../model/UsuarioDAO.php');
require_once ('../model/Pessoas.php');
require_once ('../model/PessoasDAO.php');
imprime_menu(); 
$db = new Database(); 
$dao = new ProdutosDAO($db); 
$produtos=$dao->buscarTodos();
$dao = new PessoasDAO($db);
$pessoas=$dao->buscarTodos();
?> 

<html lang="pt-br"> 
<head>  
<title>Cadastro Movimentação </title> 
  <link rel="stylesheet" href="../view/estilo_movimentacao_produto.css"> 
</head> 

<body>
	<form method="post" action="../controller/MovimentacaoController.php?action=atualiza" id="dados" name="dados">
	<div>
	<h2>Movimentação de Produtos</h2> 
    
        <label>Solicitante</label> 
        <select id="solicitante" name="solicitante" class="selectMedio">
            <?php
            echo "<option value=''>Selecione</option>";
            foreach($pessoas as $pessoa) {
                $selected = "";
                if($pessoa->getId() == $movimentacao->getSolicitante_id()){
                    $selected = "selected";
                }
                echo "<option value={$pessoa->getId()} $selected>{$pessoa->getNome()}</option>";
            }
            ?>  
        </select>
        <br>
        <label>Data</label> 
        <input type="date" name="data" id="data" style="width: 150px" value="<?=$movimentacao->getData()?>"> 
        <input type="hidden" value="<?=$_SESSION["id_usuario"]?>" name="id_usuario" id="id_usuario">
        <input type="hidden" value="<?=$movimentacao->getId()?>" name="id" id="id">
        <label class="novoLabel">Tipo</label> 
        <select required id="tipo" name="tipo" class="selectPequeno"> 
            <option value="">Selecione</option> 
            <option value="E" <?=$movimentacao->getTipo() == "E" ? "selected" : ""?>>Entrada</option> 
            <option value="S" <?=$movimentacao->getTipo() == "S" ? "selected" : ""?>>Saída</option> 
        </select> 
        <br>
         <label id="origem1" style="display: none">Origem </label> 
        <select id="origem" name="origem" style="display: none" class="selectPequeno"> 
            <option value="">Selecione</option> 
            <option value="D" <?=$movimentacao->getOrigem() == "D" ? "selected" : ""?>>Doação</option> 
            <option value="C" <?=$movimentacao->getOrigem() == "C" ? "selected" : ""?>>Compra</option>
        </select>
        <label id="doador" style= "display:none">Doador </label> 
        <select id="doadores" name="doadores" style="display: none" class="selectPequeno">
            <?php
            echo "<option value=''>Selecione</option>";
            foreach($pessoas as $pessoa) {
                $selected = "";
                if($pessoa->getId() == $movimentacao->getDoador_id()){
                    $selected = "selected";
                }
                echo "<option value={$pessoa->getId()} $selected>{$pessoa->getNome()}</option>";
            }
            ?>  
        </select>
        <table id="tabela_produtos">
             <tr>
                 <td width="65%" align="center"><b>PRODUTOS</b></td>
                 <td align="left"><b>QUANTIDADE</b></td>
             </tr>
             <?php
                foreach ($movimentacao->getProdutos() as $prod) {
                    ?>
                  <tr>
                    <td  width="65%">
                       <select required id="produto[]" name="produto[]" class="selectGrande">
                           <?php
                            echo "<option value={$prod->getId()} selected>{$prod->getNome()}</option>";
                            ?>  
                       </select>  
                    </td>
                    <td>
                        <input id="quantidade[]" name="quantidade[]" type="text" maxlength="5" size="30" class="inputPequeno" disabled="" value="<?=$prod->getQuantidade_movimentacao()?>">
                    </td>
                </tr>
                    <?php
                }
                ?>
         </table>
        <br><br>
        <button class="btnSalvar">Salvar</button>
        <button class="btnCancelar" type="button" name="voltar" id="voltar" onclick="window.location='../controller/MovimentacaoController.php?action=lista'">Cancelar</button>
        </div>
    </form>
</body> 
</html> 
<script> 
    $(document).ready(function (){ 
        if($("#tipo").val() == "E"){ 
            $("#origem").show(); 
            $("#origem1").show();
        } else { 
            $("#origem").hide(); 
            $("#origem1").hide(); 
        } 
        if($("#origem").val() == "D"){ 
            $("#doador").show(); 
            $("#doadores").show();
        } else { 
            $("#doador").hide(); 
            $("#doadores").hide(); 
        } 
    }); 
</script>

