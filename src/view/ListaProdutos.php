<?php
include_once "../../funcoes.php";
require_once ('../model/Classificacao.php');
require_once ('../model/ClassificacaoDAO.php');
require_once ('../config/Database.php');
imprime_menu();
?>
<html>
<head>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>﻿
<script type="text/javascript">

function deleteRow(row,produto){
	if(confirm("Tem certeza que deseja apagar?")){
	$.post('../controller/ProdutosController.php?action=remove',{postproduto:produto},
            function(data)
            {
            if(data){
            //apaga a linha da tabela em html
            row.parentNode.parentNode.parentNode.parentNode.deleteRow(row.parentNode.parentNode.rowIndex);
            }else{
                alert("Algum problema ocorreu. Recarregue a página novamente");
                }
            });
	}

}
</script>  
<meta>
<title>Produtos</title>
<link rel="stylesheet" href="../view/estilo.css">
</head>
<body>
	<form method="post" class="form" name="formcar" id="formcar" action="ProdutosController.php?action=filtrar">
		<h2>Listagem de Produtos</h2>
		<select class="selectFiltro" id="campo" name="campo" onchange="changeSelect()">
			<option value="Nome">Descricao</option>
		</select>
		<select class="selectFiltro" id="tipo" name="tipo" onchange="changeSelect()">
			<option value="">Contenha</option>
			</select>
		<input class="campoFiltro" type="text" name="busca" placeholder="Buscar..." >
		<button class="btnBuscar">Buscar</button>
		<table id = 'dsTable'>
			<thead>
                            <tr style="background: none">
				<th>Descrição</th>
				<th>Classificação</th> 
				<th>Quantidade Mínima</th>
				<th width="60">Ações</th>
				</tr>
			</thead>
			 <?php   
       if(isset($produtos) && !empty($produtos)) {
            foreach($produtos as $produto) {
                
                $db = new Database();
                $dao = new ClassificacaoDAO($db);
                $classificacoes=$dao->buscar_id($produto->getClassificacao());
             
                echo "<tr id=tr_{$produto->getId()}>
                <td>{$produto->getNome()}</td>
                <td>{$classificacoes->getDescricao()}</td>
                <td>{$produto->getQuantidade_minima()}</td>
                <td><a href='#' onClick='deleteRow(this,{$produto->getId()})'><img src='../view/imagens/lixeira24.png' /></a>
                <a href='../controller/ProdutosController.php?action=edita&idproduto={$produto->getId()}' ><img src='../view/imagens/lapis24.png' /></a>
                </td>
                </tr>";
            }
        } else {
            echo "<tr><td colspan=4 align='center'><font size=3>Não há produtos cadastrados</font></td></tr>";
        }
        ?>  
			
		</table>
	</form>

</body>
</html>