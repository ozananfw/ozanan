<?php
include_once "../../funcoes.php";
require_once ('../config/Database.php');
require_once ('../model/Pessoas.php');
require_once ('../model/PessoasDAO.php');
require_once ('../model/IdosoDAO.php');
require_once ('../model/ResponsavelDAO.php');
imprime_menu();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Cadastro de Responsáveis</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="javascriptpersonalizado.js"></script>
  <link rel="stylesheet" href="../view/estiloResponsavel.css">
  
</head>
<body class="form_diferenciado">
<div class="container" id = "DIVcontainer">
  <h2>Cadastro de Responsáveis</h2>
  <?php
	//session_start();
	$ididoso = $_SESSION['idosoID'];
	$db = new Database();
	$dao = new IdosoDAO($db);
	$pessoa = $dao->buscaPorIdoso($ididoso);
	echo "<h2>Paciente: {$pessoa->getNome()}</h2>";
	
?>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Adicionar Responsável</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Adicionar Responsável</h4>
        </div>
        <div class="modal-body">
		<form method="POST" id="form-pesquisa" action="">
			Nome: <input type="text" name="pesquisa" id="pesquisa" placeholder="Quem você está procurando?">
		</form>
		
		<ul class="resultado">
		
		</ul>
		</table>
		
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>    
    </div>
  </div>
  
  	<div class='divtabela'>
	 <table id = 'dsTable'>
	<thead>
        <tr style="background: none">
		<th>Nome</th>
		<th width="30">Ações</th>
		</tr>
	</thead>
	<?php   
	$db = new Database();
	$dao = new ResponsavelDAO($db);
	$ididoso = $_SESSION['idosoID'];
	$responsaveis = $dao->buscaPorIdoso($ididoso);
	
    if(isset($responsaveis) && !empty($responsaveis)) {
        foreach($responsaveis as $responsavel) {
		$db = new Database();
        echo "<tr id=tr_{$responsavel->getId()}>
              <td>{$responsavel->getNome()}</td>
              <td><a href='../controller/IdosoController.php?action=removeResponsavel&ididoso={$ididoso}&idresponsavel={$responsavel->getId()}' title='Remover Responsável'><img src='../view/imagens/lixeira24.png' /></a>
              </td>
              </tr>";
        }
    } else {
      echo "<tr><td colspan=4 align='center'><font size=3>Não há responsaveis cadastrados</font></td></tr>";
    }
    ?>
	</div>
</div>
</body>
</html>