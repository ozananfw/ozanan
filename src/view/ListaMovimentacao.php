<?php
include_once "../../funcoes.php";
require_once ('../model/Movimentacao.php');
require_once ('../model/MovimentacaoDAO.php');
require_once ('../model/Funcionario.php');
require_once ('../model/FuncionarioDAO.php');
require_once ('../model/Pessoas.php');
require_once ('../model/PessoasDAO.php');
require_once ('../config/Database.php');
imprime_menu();
?>
<html>
<head>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>﻿
<script type="text/javascript">

function deleteRow(row,movimentacao){
	if(confirm("Tem certeza que deseja apagar?")){
	$.post('../controller/MovimentacaoController.php?action=remove',{postmovimentacao:movimentacao},
            function(data)
            {
            if(data){
            //apaga a linha da tabela em html
            row.parentNode.parentNode.parentNode.parentNode.deleteRow(row.parentNode.parentNode.rowIndex);
            }else{
                alert("Algum problema ocorreu. Recarregue a página novamente");
                }
            });
	}

}
</script>  
<meta>
<title>Movimentações</title>
<link rel="stylesheet" href="../view/estilo.css">
</head>
<body>
    <form method="post" class="form" action="MovimentacaoController.php?action=filtrar">
        <h2>Listagem de Movimentações</h2>
        <select class="selectFiltro" id="campo" name="campo" onchange="changeSelect()">
                <option value="solicitante">Solicitante</option>
                <option value="responsavel">Responsável</option>
                <option value="data">Data</option>
        </select>
        <input class="campoFiltro" type="text" name="busca" placeholder="Buscar..." >
        <button class="btnBuscar">Buscar</button>
        <table id = 'dsTable'>
            <thead>
                <tr style="background: none">
                    <th>Solicitante</th>
                    <th>Responsável pelo cadastro</th>
                    <th>Data</th> 
                    <th>Tipo</th>
                    <th width="60">Ações</th>
                    </tr>
            </thead>
            <?php   
            if(isset($movimentacoes) && !empty($movimentacoes)) {
//                print_r($movimentacoes);
                foreach($movimentacoes as $mov) {
                    $db = new Database();
                    $dao = new MovimentacaoDAO($db);
                    $pessoa_dao = new PessoasDAO($db);
                    $pessoa = $pessoa_dao->busca($mov->getSolicitante_id());
                    $funcionarioDAO = new FuncionarioDAO($db);
                    $funcionario = $funcionarioDAO->buscar($mov->getFuncionario_id());
                    $tipo = "Saída";
                    if($mov->getTipo() == "E") {
                        $tipo = "Entrada";
                    }
                    echo "<tr id=tr_".$mov->getId().">
                    <td>".strtoupper($pessoa->getNome())."</td>
                    <td>".strtoupper($funcionario->getNome())."</td>
                    <td align='center'>".dataBR($mov->getData())."</td>
                    <td align='center'>$tipo</td>
                    <td><a href='#' onClick='deleteRow(this,{$mov->getId()})'><img src='../view/imagens/lixeira24.png' /></a>
                    <a href='../controller/MovimentacaoController.php?action=edita&idmovimentacao={$mov->getId()}' ><img src='../view/imagens/lapis24.png' /></a>
                    </td>
                    </tr>";
                }
            } else {
                echo "<tr><td colspan=4 align='center'><font size=3>Não há produtos cadastrados</font></td></tr>";
            }
            ?>  
        </table>
    </form>

</body>
</html>