<?php
include_once "../../funcoes.php";
imprime_menu();
?>
<html>
<head>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>﻿
<script type="text/javascript">
function deleteRow(row,carro){

	if(confirm("Tem certeza que deseja apagar?")){
	$.post('../controller/CarroController.php?action=remove',{postcarro:carro},
            function(data)
            {
            if(data){
            //apaga a linha da tabela em html
            row.parentNode.parentNode.parentNode.parentNode.deleteRow(row.parentNode.parentNode.rowIndex);
            }else{
                alert("Algum problema ocorreu. Recarregue a página novamente");
                }
            });
	}

}
</script>  
<meta>
<title>Carros</title>
<link rel="stylesheet" href="../view/estilo.css">
</head>
<body>
	<form class="form" name="formcar" id="formcar">
		<h2>Listagem de Carros</h2>
		<select class="selectFiltro" id="campo" name="campo" onchange="changeSelect()">
			<option value="">Campo:</option>
			<option value="Nome">Nome</option>
			<option value="Telefone">Telefone</option>
		</select>
		<select class="selectFiltro" id="tipo" name="tipo" onchange="changeSelect()">
			<option value="">Tipo:</option>
			<option value="idoso">Idoso</option>
			<option value="funcionario">Funcionário</option>
			<option value="fornecedor">Fornecedor</option>
		</select>
		<input class="campoFiltro" type="text" name="busca" placeholder="Buscar..." >
		<button class="btnBuscar">Buscar</button>
		<table id = 'dsTable'>
			<thead>
				<th>Placa</th>
                <th>Responsavel</th>
                <th>KM Saida</th>
                <th>KM Chegada</th>
                <th>Data Saida</th>
                <th>Data Chegada</th>
                <th width="60">Ações</th>
			</thead>
			 <?php
       if(isset($mVeiculo) && !empty($mVeiculo)) {
            foreach($mVeiculo as $value) {
            echo "<tr id=tr_{$value->getId()}>
                <td>{$value->getVeiculo()}</td>
                <td>{$value->getPessoa()}</td>
                <td>{$value->getKmsaida()}</td>
                <td>{$value->getKmEntrda()}</td>
                <td>{$value->getDSaida()}</td>
                <td>{$value->getDentrada()}</td>
                <td>
                <a href='../view/form_veiculo_entrada.php?id={$value->getId()}' ><img src='../view/imagens/lapis24.png' /></a>
                </td>
                </tr>";
            }
        } else {
            echo "<tr><td colspan=8 align='center'><font size=3>Não há carros cadastrados</font></td></tr>";
        }
        ?>

		</table>

        <button onclick="" type="button" class="btnCancelar">Cancelar</button>
        <button type="button" onclick="trocar_tela('index')" class="btnSalvar">Incluir</button>
	</form>

<script>
 function trocar_tela(url){
     window.location.href="../view/form_veiculo_saida.php";
 }


</script>
</body>
</html>