<?php
include_once "../../funcoes.php";
imprime_menu();
?>
<html lang="pt-br">
<head>
<meta charset='iso-8859-15'/>
	<title>cadastro de pessoas </title>
        <link rel="stylesheet" href="estilo_movimentacao_produto.css">
	
<script type="text/javascript">
function adiciona() {
            var div = document.getElementById("tabela_endereco");
            div.innerHTML += `<br><br><tr>
                                        <td>
                                            <label>Rua</label><br><input id="rua[]" name="rua[]" type="text" placeholder="Digite o nome da rua " maxlength="50" size="30" class="inputMedio"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Numero</label><label style="margin-left: 200px">Bairro</label><br><input id="numero[]" name="numero[]" type="text" placeholder="Digite o numero da casa " maxlength="5" size="30" style="width: 20%;"/>
                                            <input id="bairro[]" name="bairro[]" type="text" placeholder="Digite o bairro " maxlength="30" size="30" style="margin-left: 100px; width: 40%"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Cidade</label><label style="margin-left: 300px">Estado</label><br><input id="cidade[]" name="cidade[]" type="text" placeholder="Digite a cidade" maxlength="50" size="30" class="inputPequeno"/>
                                            
                                            <select id="estado[]" name="estado[]" class="selectPequeno">
                                                    <option value="MG">Minas Gerais</option>
                                                    <option value="SP">São Paulo</option>
                                                    <option value="RJ">Rio de Janeiro</option>
                                                    <option value="ES">Espirito Santo</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>CEP</label><br><input id="cep[]" name="cep[]" type="text" placeholder="Digite o cep" maxlength="8" size="8" class="inputPequeno"/>
                                        </td>
                                    </tr>
                                    <section id="insere" style="background: none"> </section>`;
        }
		
function adicionaTel(){
		var div = document.getElementById("insereTel");
		div.innerHTML += '<input id="tel[]" name="tel[]" type="text" placeholder="Digite o numero de telefone" maxlength="15" size="30"/>'
	}
function adicionaEmail(){
		var div = document.getElementById("insereEmail");
		div.innerHTML += '<input id="email[]" name="email[]" type="text" placeholder="Digite o email " maxlength="50" size="45"/>'
	}
	
 $(document).ready(function(){
     $('#tel').mask('(00) 0000-0000');
  });
</script>

</head>
<body>
	<form method="post" action="../controller/PessoaController.php?action=adiciona" id="dados" name="dados">
            <div class="divMedia">
			<h2>Cadastro de Pessoas</h2>
                        <br>
				<label>Dados Pessoais</label>
                                <br><br>
				<label>Nome completo</label><br><input id="name" name="nome" type="text" autofocus placeholder="Digite o nome completo" maxlength="50" size="30"/>      
                                <label>CPF</label><label class="novoLabel" style="margin-left: 300px">Sexo</label><br><input id="cpf" name="cpf" type="text" placeholder="Digite o CPF" maxlength="15" size="30" class="inputPequeno"/>
                                
                                <select id="sexo" name="sexo" class="selectPequeno">
					<option value="Null">Selecione</option>
					<option value="M">Masculino</option>
					<option value="F">Feminino</option>
				</select>
                                <br>
                                <label>Data de nascimento</label> <input id="data" name="data" value="data" type="date" /><br>
                                <br>
                                <table>
                                    <tr>
                                        <td align="center">CONTATO(s)</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href='#' onClick='adicionaEmail()'><img src='../view/imagens/mais24.png' id = "imgAdd"/></a><label style="margin-left: 20px">E-mail</label><br><input id="email[]" name="email[]" type="text" placeholder="Digite o email " maxlength="50" size="45" class="inputMedio"/>
                                            <section id="insereEmail" style="background: none"> </section>

                                            <a href='#' onClick='adicionaTel()'><img src='../view/imagens/mais24.png' id = "imgAdd"/></a><label style="margin-left: 20px">Telefone</label><br><input  id="tel[]" name="tel[]" placeholder="Digite o numero de telefone" type="text" maxlength="15" size="30" class="inputPequeno"/>
                                            <section id="insereTel" style="background: none"> </section>
                                            
                                        </td>
                                    </tr>
                                </table>
                                <br>
                                <table id='tabela_endereco'>
                                    <tr>
                                        <td align="center">ENDEREÇO(s)&nbsp;&nbsp;&nbsp;<a href='#' onClick='adiciona()'><img src='../view/imagens/mais24.png' /></a></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Rua</label><br><input id="rua[]" name="rua[]" type="text" placeholder="Digite o nome da rua " maxlength="50" size="30" class="inputMedio"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Numero</label><label style="margin-left: 200px">Bairro</label><br><input id="numero[]" name="numero[]" type="text" placeholder="Digite o numero da casa " maxlength="5" size="30" style="width: 20%;"/>
                                            <input id="bairro[]" name="bairro[]" type="text" placeholder="Digite o bairro " maxlength="30" size="30" style="margin-left: 100px; width: 40%"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Cidade</label><label style="margin-left: 300px">Estado</label><br><input id="cidade[]" name="cidade[]" type="text" placeholder="Digite a cidade" maxlength="50" size="30" class="inputPequeno"/>
                                            
                                            <select id="estado[]" name="estado[]" class="selectPequeno">
                                                    <option value="MG">Minas Gerais</option>
                                                    <option value="SP">São Paulo</option>
                                                    <option value="RJ">Rio de Janeiro</option>
                                                    <option value="ES">Espirito Santo</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>CEP</label><br><input id="cep[]" name="cep[]" type="text" placeholder="Digite o cep" maxlength="8" size="8" class="inputPequeno"/>
                                        </td>
                                    </tr>
                                </table>
				
				<label>Observações</label><br><textarea id="obs" name="obs" type="textarea" rows="5" cols="60" placeholder="Digite uma Observação sobre a pessoa" maxlength="100"> </textarea>
			<br><br>
			<button class="btnCancelar" type="button" name="voltar" id="voltar" onclick="window.location='../controller/PessoaController.php?action=lista'">Cancelar</button>
			<button class="btnSalvar" type="input">Salvar</button>
	</div>
	</form>
</body>
</html>