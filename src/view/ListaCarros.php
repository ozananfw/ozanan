<?php
include_once "../../funcoes.php";
imprime_menu();
?>
<html>
<head>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>﻿
<script type="text/javascript">
function deleteRow(row,carro){

	if(confirm("Tem certeza que deseja apagar?")){
	$.post('../controller/CarroController.php?action=remove',{postcarro:carro},
            function(data)
            {
            if(data){
            //apaga a linha da tabela em html
            row.parentNode.parentNode.parentNode.parentNode.deleteRow(row.parentNode.parentNode.rowIndex);
            }else{
                alert("Algum problema ocorreu. Recarregue a página novamente");
                }
            });
	}

}
</script>  
<meta>
<title>Carros</title>
<link rel="stylesheet" href="../view/estilo.css">
</head>
<body>
	<form class="form" name="formcar" id="formcar">
		<h2>Listagem de Carros</h2>
		<select class="selectFiltro" id="campo" name="campo" onchange="changeSelect()">
			<option value="">Campo:</option>
			<option value="Nome">Nome</option>
			<option value="Telefone">Telefone</option>
		</select>
		<select class="selectFiltro" id="tipo" name="tipo" onchange="changeSelect()">
			<option value="">Tipo:</option>
			<option value="idoso">Idoso</option>
			<option value="funcionario">Funcionário</option>
			<option value="fornecedor">Fornecedor</option>
		</select>
		<input class="campoFiltro" type="text" name="busca" placeholder="Buscar..." >
		<button class="btnBuscar">Buscar</button>
		<table id = 'dsTable'>
			<thead>
                            <tr style="background: none">
				<th>Nome</th>
				<th>Placa</th> 
				<th>Status</th>
				<th width="60">Ações</th>
				</tr>
			</thead>
			 <?php   
       if(isset($carros) && !empty($carros)) {
            foreach($carros as $carro) {
            echo "<tr id=tr_{$carro->getId()}>
                <td>{$carro->getNome()}</td>
                <td>{$carro->getPlaca()}</td>
                <td>{$carro->getStatus()}</td>
                <td><a href='#' onClick='deleteRow(this,{$carro->getId()})'><img src='../view/imagens/lixeira24.png' /></a>
                <a href='../controller/CarroController.php?action=edita&idcarro={$carro->getId()}' ><img src='../view/imagens/lapis24.png' /></a>
                </td>
                </tr>";
            }
        } else {
            echo "<tr><td colspan=4 align='center'><font size=3>Não há carros cadastrados</font></td></tr>";
        }
        ?>  
			
		</table>
	</form>

</body>
</html>