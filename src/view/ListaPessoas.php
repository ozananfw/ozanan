<?php
include_once "../../funcoes.php";
imprime_menu();
?>
<html>
<head>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>﻿
<script type="text/javascript">
function deleteRow(row,pessoa){

	if(confirm("Tem certeza que deseja apagar?")){
	$.post('../controller/PessoaController.php?action=remove',{postpessoa:pessoa},
            function(data)
            {
            if(data){
            //apaga a linha da tabela em html
            row.parentNode.parentNode.parentNode.parentNode.deleteRow(row.parentNode.parentNode.rowIndex);
            }else{
                alert("Algum problema ocorreu. Recarregue a p�gina novamente");
                }
            });
	}

}
</script>  
<meta>
<title>Pessoas</title>
<link rel="stylesheet" href="../view/estilo.css">
</head>
<body>
	<form method="post" action="../controller/PessoaController.php?action=filtrar" id="lista" name="lista">
		<h2>Listagem de Pessoas</h2>
		<select class="selectFiltro" id="campo" name="campo" onchange="changeSelect()">
			<option value="Nome">Nome</option>
			<option value="Telefone">Telefone</option>
			<option value="Email">Email</option>
		</select>
		<select class="selectFiltro" id="tipo" name="tipo" onchange="changeSelect()">
			<option value="">Contenha</option>
		</select>
		<input class="campoFiltro" type="text" name="busca" placeholder="Buscar..." >
		<button class="btnBuscar">Buscar</button>
		<table id = 'dsTable'>
			<thead>
                            <tr style="background: none">
				<th>Nome</th>
				<th>Telefone</th> 
				<th width="60">A��es</th>
				</tr>
			</thead>
			 <?php   
       if(isset($pessoas) && !empty($pessoas)) {
            foreach($pessoas as $pessoa) {
			$db = new Database();
			$comunicaTelefone = new TelefoneDAO($db);
			$telefones = $comunicaTelefone->buscaPorPessoa($pessoa->getId());
			if(isset($telefones) && !empty($telefones)) {
				foreach($telefones as $RegistroTel){
					$tel =$RegistroTel->getTelefone();
					break;
				}
			} else {
				$tel = '';
			}
            echo "<tr id=tr_{$pessoa->getId()}>
                <td>{$pessoa->getNome()}</td>
                <td>{$tel}</td>
                <td><a href='#' onClick='deleteRow(this,{$pessoa->getId()})' title='Remover Pessoa'><img src='../view/imagens/lixeira24.png' /></a>
                <a href='../controller/PessoaController.php?action=edita&idpessoa={$pessoa->getId()}' title='Editar Pessoa'><img src='../view/imagens/lapis24.png' /></a>
                </td>
                </tr>";
            }
        } else {
            echo "<tr><td colspan=4 align='center'><font size=3>Não há pessoas cadastrados</font></td></tr>";
        }
        ?>  
			
		</table>
	</form>

</body>
</html>