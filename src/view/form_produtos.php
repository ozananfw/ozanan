<?php


include_once "../../funcoes.php";
require_once ('../model/Classificacao.php');
require_once ('../model/ClassificacaoDAO.php');
require_once ('../config/Database.php');
imprime_menu();
?>
<html lang="pt-br">

<head>
<title>Cadastro Produtos </title>
	<link rel="stylesheet" href="estilo.css">
</head>

<body>
	<form method="post" action="../controller/ProdutosController.php?action=adiciona" id="dados" name="dados">
	<div>
		<h2>Cadastre as Produtos</h2>

       <!-- <input name="action" type="hidden" value="adiciona"> -->
            <label for="nome">Descrição</label>
            <input type="text" name="nome" id="nome">
        
            <label for="quantidade">Quantidade Mínima</label>
            <input style="width: 50px" type="number" name="quantidade_minima" id="quantidade_minima">
              <!-- <br> Remover -->
            
            <label>Categoria</label>
            <select required="" id="classificacao" name="classificacao">
					<?php
					$db = new Database();
					$dao = new ClassificacaoDAO($db);
					$classificacoes=$dao->buscarTodos();
       if(isset($classificacoes) && !empty($classificacoes)) {
           echo "<option value=''>Selecione</option>";
            foreach($classificacoes as $classificacao) {
                echo "<option value={$classificacao->getId()}>{$classificacao->getDescricao()}</option>";
            }
        } else {
            echo "<tr><td colspan=4 align='center'><font size=3>Não há classificacões cadastradas</font></td></tr>";
        }
        ?>  
        </select>
        
                <button class="btnSalvar">Salvar</button>
		<button class="btnCancelar" type="button" name="voltar" id="voltar" onclick="window.location='../controller/ProdutosController.php?action=lista'">Cancelar</button>
          
      </div>
    </form>


</body>

</html>