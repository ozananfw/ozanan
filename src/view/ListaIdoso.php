<?php
include_once "../../funcoes.php";
imprime_menu();
?>
<html>
<head>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>﻿
<script type="text/javascript">
function deleteRow(row,idoso){

	if(confirm("Tem certeza que deseja apagar?")){
	$.post('../controller/IdosoController.php?action=remove',{postidoso:idoso},
            function(data)
            {
            if(data){
            //apaga a linha da tabela em html
            row.parentNode.parentNode.parentNode.parentNode.deleteRow(row.parentNode.parentNode.rowIndex);
            }else{
                alert("Algum problema ocorreu. Recarregue a página novamente");
                }
            });
	}

}
</script>  
<meta>
<title>Idosos</title>
<link rel="stylesheet" href="../view/estilo.css">
</head>
<body>
	<form method="post" action="../controller/PessoaController.php?action=filtrar" id="lista" name="lista">
		<h2>Listagem de Idosos</h2>
		<select class="selectFiltro" id="campo" name="campo" onchange="changeSelect()">
			<option value="Nome">Nome</option>
			<option value="Telefone">Responsável</option>
			<option value="Email">Medicamento</option>
		</select>
		<select class="selectFiltro" id="tipo" name="tipo" onchange="changeSelect()">
			<option value="">Contenha</option>
		</select>
		<input class="campoFiltro" type="text" name="busca" placeholder="Buscar..." >
		<button class="btnBuscar">Buscar</button>
		<table id = 'dsTable'>
			<thead>
                <tr style="background: none">
				<th>Nome</th>
				<th width="110">Ações</th>
				</tr>
			</thead>
			 <?php   
       if(isset($idosos) && !empty($idosos)) {
            foreach($idosos as $idoso) {
			$db = new Database();
            echo "<tr id=tr_{$idoso->getId()}>
                <td>{$idoso->getNome()}</td>
                <td><a href='#' onClick='deleteRow(this,{$idoso->getPessoa_id()})' title='Remover Idoso'><img src='../view/imagens/lixeira24.png' /></a>
                <a href='../controller/IdosoController.php?action=edita&idpessoa={$idoso->getPessoa_id()}' title='Editar Idoso' ><img src='../view/imagens/lapis24.png' /></a>
				<a href='../controller/IdosoController.php?action=editaResponsaveis&ididoso={$idoso->getId()}' title='Listar Responsáveis'><img src='../view/imagens/reponsavel24.png' /></a>
				<a href='../controller/IdosoController.php?action=listaMedicamentos&ididoso={$idoso->getId()}' title='Listar Medicamentos'><img src='../view/imagens/remedio.png' /></a>
                </td>
                </tr>";
            }
        } else {
            echo "<tr><td colspan=4 align='center'><font size=3>Não há idosos cadastrados</font></td></tr>";
        }
        ?>  
			
		</table>
	</form>

</body>
</html>