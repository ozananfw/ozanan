<?php


include_once "../../funcoes.php";
require_once ('../model/Classificacao.php');
require_once ('../model/ClassificacaoDAO.php');
require_once ('../config/Database.php');
require_once ('../model/Pessoas.php');
require_once "../model/PessoasDao.php";
require_once ('../config/Database.php');
require_once "../model/CarroDAO.php";
require_once "../model/Carro.php";
imprime_menu();

$db = new Database();
$dao = new PessoasDAO($db);
$pessoas = $dao->buscarTodos();
$daoCarro = new CarroDAO($db);
$carro = $daoCarro->buscarTodos();

?>



<html lang="pt-br">

<head>
<title>Cadastro Produtos </title>
	<link rel="stylesheet" href="estilo.css">
    <style>
        #kmsaida{
            width: 50px;
        }
    </style>
</head>

<body>
	<form method="post" action="../controller/MVeiculoController.php?action=adiciona" id="dados" name="dados">
	<div>
		<h2>Saida de Veiculos</h2>


        <label>Motorista Responsavel</label>
        <select name="idFun">
            <option value="null">Escolha Uma Pessoa</option>
            <?php
            foreach ($pessoas as $key => $value) {
                $nome = $value->getNome();
                $id = $value->getId();

                echo '<option value=' . $id . '>';
                echo $nome;
                echo '</option>';
            }
            ?>
        </select>
        <label>Veiculo</label>
        <select name="idCar">
            <option value="null">Placa</option>
            <?php
            foreach ($carro as $key => $value) {
                $nome = $value->getPlaca();
                $id = $value->getId();
                echo '<option value=' . $id . '>';
                echo $nome;
                echo '</option>';
            }
            ?>
        </select>
        <label for="nome">Data</label>
        <input type="date" name="data" id="data" style="width: 150px" value="<?=date('d')?>">
        <label for="nome">Hora</label>
        <input type="time" name="hora" id="data" style="width: 150px" value="<?=time()?>">

       <!-- <input name="action" type="hidden" value="adiciona"> -->
            <label for="nome">Motivo</label>
            <input type="text" name="motivo" id="nome">
            <label for="nome">Observação</label>
            <input type="text" name="obs" id="nome">
        
            <label for="kmSaida">KM de Saida</label>
            <input  style="width: 50px" type="number" name="kmSaida" id="kmsaida">
              <!-- <br> Remover -->
                <button class="btnSalvar">Incluir</button>
		        <button class="btnCancelar" type="button" name="voltar" id="voltar" onclick="window.location='../controller/MVeiculoController?action=lista'">Cancelar</button>
          
      </div>
    </form>


</body>

</html>